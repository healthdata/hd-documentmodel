/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');

var defaultTrait = {
  requires: [ 'field' ],
  provides: [ 'defaultValue' ],
  behaviour: {
    hasDefaultValue: function (description) {
      return _.has(description, 'defaultValue') ||
             _.has(description, 'computedDefaultValue');
    },
    defaultValue: function (manipulator) {
      var description = manipulator.description;
      if (_.has(description, 'defaultValue')) {
        return description.defaultValue;
      }
      if (_.has(description, 'computedDefaultValue')) {
        return manipulator.computedExpressions[description.computedDefaultValue];
      }
      return; // no default value
    }
  },
  contributes: [
    {
      name: 'descriptionBuilders',
      value: skrDefault
    }
  ]
};

module.exports = defaultTrait;

function skrDefault(skrDescription, config) {
  if (_.has(config, 'default')) {
    skrDescription.setDefaultValue(config.default);
  } else if (_.has(config, 'computedDefault')) {
    skrDescription.computedDefaultValue = config.computedDefault;
  }
}
