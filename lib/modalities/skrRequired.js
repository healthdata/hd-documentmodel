/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var requiredTrait = {
  requires: [ 'field' ],
  provides: [ 'required' ],
  contributes: [
    {
      name: 'descriptionBuilders',
      value: skrRequired
    }
  ]
};

module.exports = requiredTrait;

/**
 * Configure a description with an assertion that checks for presence of a value.
 * @param  {Description}    skrDescription the description to be configured
 * @param  {Boolean|String} isRequired     whether required; can be the name of a
 *                                         computed expression
 */
function skrRequired(skrDescription, config) {
  var isRequired = config.required;
  if (isRequired) {
    var isConditional = typeof isRequired === 'string';

    // install an 'isRequired' property on the manipulator
    // for conditional requiredness this property's value is computed
    if (isConditional) {
      var conditionName = isRequired;
      skrDescription.addManipulatorBuilder(function (manipulator) {
        Object.defineProperty(manipulator, 'isRequired', {
          get: function () {
            // any value other than false means the field is required
            return manipulator.computedExpression(conditionName) !== false;
          }
        });
      });
    } else {
      skrDescription.addManipulatorBuilder(function (manipulator) {
        manipulator.isRequired = true;
      });
    }

    // define an assertions, which works for both conditional and
    // unconditional requiredness
    skrDescription.assertions.required = {
      check: function checkRequired(data, manipulator) {
        if (isConditional &&
            manipulator &&
            !manipulator.isRequired) {
          return 'ok';
        }
        var failed = skrDescription.isAbsent(data, manipulator) ||
                     skrDescription.isEmpty(data, manipulator);
        return failed ? 'failed' : 'ok';
      }
    };
    skrDescription.messages.required = 'required field';
  }
}
