/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');
var helpers = require('../core/helpers');

var conditionsTrait = {
  requires: [ 'field' ],
  contributes: [
    {
      name: 'descriptionBuilders',
      value: skrConditions
    }
  ]
};

module.exports = conditionsTrait;

function skrCondition(skrDescription, config) {
  helpers.defineCondition(skrDescription,
    config.name, config.expression,
    config.errorMessage, config.level);
  setupInterfieldErrorMessage(skrDescription, config.name);
}

function skrConditions(skrDescription, config) {
  _.each(config.conditions, function (condition) {
    skrCondition(skrDescription, condition);
  });
}

function setupInterfieldErrorMessage(skrDescription, name) {

  skrDescription.assertions[name].isInterField = true;

  skrDescription.addManipulatorBuilder(function (manipulator) {
    manipulator.parseReference = function (reference) {
      // On regex matching:
      // http://stackoverflow.com/questions/19913667/javascript-regex-global-match-groups
      // We use the following regular expression to extract the property names
      // from a reference like ['abc']['xy_uv']
      var re =  /\['([^']+)'\]/g;
      var propertyNames = [];
      var matches;
      while ((matches = re.exec(reference))) {
        propertyNames.push(matches[1]);
      }
      return propertyNames;
    };

    // TODO Reuse this function from the document model instead of copying it here.
    function pathAsString(path) {
      return _.reduce(path, function (result, propertyName) {
        return result + '.' + propertyName;
      }, '%');
    }

    manipulator.resolveReference = function (reference) {
      var propertyNames = manipulator.parseReference(reference);
      var path = manipulator.path;
      var referencePath = path.concat(propertyNames);
      var key = pathAsString(referencePath);
      var result = manipulator.document.registeredManipulators[key];
      return result;
    };

    // this function "folds" over the message, matching all references
    manipulator.processMessage = function (message, processMatch) {
      var referenceRE = /\{\{([^\|]+)\|([^\}]+)\}\}/g;
      var match;
      while ((match = referenceRE.exec(message))) {
        var label = match[1];
        var reference = match[2];
        processMatch(match[0], label, manipulator.resolveReference(reference));
      }
    };

    var message = skrDescription.messageFor(name);
    if (message) {
       manipulator.processMessage(message,
        function (match, label, linkedManipulator) {
          setupInterFieldValidationLink(linkedManipulator);
        });
    }

    function setupInterFieldValidationLink(linkedManipulator) {
      if (linkedManipulator) {
        var linked = linkedManipulator.linked;
        if (!linked) {
          linked = linkedManipulator.linked = [];
        }
        linked.push({
          manipulator: manipulator,
          assertion: name
        });
      }
    }
  });
}
