/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');
var skrComputedBase = require('./skrComputedBase');

var valuesTrait = {
  requires: [ 'field' ],
  provides: [ 'computedValues' ],
  contributes: [
    {
      name: 'descriptionBuilders',
      value: compileValues
    }
  ]
};

module.exports = valuesTrait;

function skrValues(skrDescription, config) {
  var listName = config.listName;
  var propName = config.propName;

  function buidState(manipulator) {
    var memoized = manipulator.computedExpressionsMemoized[listName];
    return {
      computedValue: function () {
        var valuesList = memoized.value();
        var propsList = _.uniq(_.map(valuesList, function (value) {
          return value[propName];
        }));
        return propsList.length === 1 ? propsList[0] : undefined;
      },
      addListener: function (f) { manipulator.onPostInvalidate(f); }
    };
  }

  skrComputedBase(buidState, skrDescription, config);
}

function compileValues(skrDescription, config) {
  if (!_.has(config, 'values')) { return; }
  var valuesConfig = config.values;
  if (!valuesConfig.length || valuesConfig.length !== 2) {
    return;
  }
  skrValues(skrDescription, {
    toggleComputed: config.toggleComputed,
    listName: valuesConfig[0],
    propName: valuesConfig[1]
  });
}
