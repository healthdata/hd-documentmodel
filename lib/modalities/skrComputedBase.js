/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');

module.exports = skrComputedBase;

/**
 * Code shared between skrComputed and skrValues
 */
function skrComputedBase(buidState, skrDescription, config) {

  var toggleComputed = skrDescription.toggleComputed = Boolean(config.toggleComputed);
  skrDescription.addManipulatorBuilder(function (manipulator) {
    manipulator.toggleComputed = toggleComputed;

    var state = buidState(manipulator);

    if (manipulator.document.readOnly) { return; }

    if (toggleComputed) {
      var refuseComputedValues = manipulator.document.metadataFor(
        'refuseComputedValues', manipulator.path);

      Object.defineProperty(manipulator, 'acceptsComputedValues', {
        get: function () { return refuseComputedValues() !== true; },
        set: function (v) {
          refuseComputedValues.set(v ? undefined : true);
          if (v) { assignComputedValue(); }
        }
      });
    }

    Object.defineProperty(manipulator, 'computedValue', {
      get: function () { return state.computedValue(); }
    });

    function computedIsEnabled() {
      if (!_.isString(config.computedWhen)) { return true; }
      var result = manipulator.computedExpressions[config.computedWhen];
      if (typeof result !== 'boolean') {
        // when the computed expression does not produce a boolean,
        // we do not compute
        return false;
      }
      return result;
    }

    // This boolean prevents infinite loops: updating manipulator.value
    // triggers an invalidation, which in turn triggers the onInvalidate
    // listener again.
    var isUpdating = false;
    function assignComputedValue() {
      // If the manipulator does not like to be assigned a computed value,
      // we skip
      if (toggleComputed && !manipulator.acceptsComputedValues) { return; }
      if (!computedIsEnabled()) { return; }
      // When the invalidation is caused by assigning the computed value to
      // manipulator.value, we do nothing.
      if (isUpdating) { return; }
      if (!manipulator.isActive()) { return; }
      isUpdating = true;
      // Only update when actually different, to avoid unneccesary invalidation.
      var newValue = state.computedValue();
      var oldValue = manipulator.read();
      // we need to pay special attention to NaN, which is the only thing in
      // JavaScript not equal to itself
      var skip = newValue === oldValue ||
                 newValue !== newValue && oldValue !== oldValue;
      if (!skip) {
        manipulator.document.inExtendedEditContext('computedValue', function () {
          manipulator.acceptComputedValue(newValue);
        });
      }
      isUpdating = false;
    }
    state.addListener(assignComputedValue);
    assignComputedValue();
  });
}
