/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');

var onlyWhenTrait = {
  requires: [ 'field' ],
  provides: [ 'onlyWhen' ],
  contributes: [
    {
      name: 'descriptionBuilders',
      value: skrOnlyWhens
    }
  ]
};

module.exports = onlyWhenTrait;

function skrOnlyWhen(skrDescription, config) {
  var reference = config.onlyWhen;
  skrDescription.isActivePredicates.push(
    function (manipulator) {
      var result = manipulator.computedExpressions[reference];
      if (typeof result !== 'boolean') {
        // when the computed expression does not produce a boolean,
        // the manipulator is active by default
        return true;
      }
      return result;
    });
}

function skrOnlyWhens(skrDescription, config) {
  var references = config.onlyWhen;
  if (_.isString(references)) {
    references = [ references ];
  }
  _.each(references, function (reference) {
    skrOnlyWhen(skrDescription, {
      onlyWhen: reference
    });
  });
}
