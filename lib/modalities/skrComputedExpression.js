/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');
var helpers = require('../core/helpers');

var computedExpressionsTrait = {
  requires: [],
  contributes: [
    {
      // For computed expressions we still use a compile step instead of a
      // description builder, because computed expressions are allowed on
      // sections, although they do not have a description of their own.
      name: 'compileSteps',
      value: {
        // We only want to do something when there actually are computed expressions.
        condition: function (env, config) { return Boolean(config.computedExpressions); },
        require: [ 'skrDescription' ],
        compile: compileComputedExpressions
      }
    }
  ]
};

module.exports = computedExpressionsTrait;

function skrComputedExpression(skrDescription, config) {
  helpers.defineComputedExpression(skrDescription,
    config.name, config.expression);
}

function compileComputedExpressions(env, config, skrDescription) {
  _.each(config.computedExpressions, function (expr, name) {
    skrComputedExpression(skrDescription, {
      name: name,
      expression: expr
    });
  });
}
