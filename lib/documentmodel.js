/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

module.exports = {
  traits: require('./traits'),
  flow: require('./flow'),
  parseDocumentDefinition: require('./core/parseDocumentDefinition'),
  autocomplete: require('./autocomplete'),
  xmlMapping: require('./xmlMapping'),
  csvMapping: require('./csvMapping'),
  components: require('./components'),
  typeTraits: {
    textTrait: require('./contentTypes/skrText'),
    dateTrait: require('./contentTypes/skrDate'),
    emailTrait: require('./contentTypes/skrEmail'),
    booleanTrait: require('./contentTypes/skrBoolean'),
    numberTrait: require('./contentTypes/skrNumber'),
    choiceTrait: require('./contentTypes/skrChoice'),
    multichoiceTrait: require('./contentTypes/skrMultiChoice'),
    optionTrait: require('./contentTypes/skrOption').optionTrait,
    rootObjectTrait: require('./contentTypes/skrObject').rootObjectTrait,
    objectTrait: require('./contentTypes/skrObject').objectTrait,
    listTrait: require('./contentTypes/skrList'),
    sectionTrait: require('./contentTypes/skrSection'),
    fieldsetTrait: require('./contentTypes/skrFieldSet')
  },
  modalityTraits: {
    computedExpressionsTrait: require('./modalities/skrComputedExpression'),
    computedWithTrait: require('./modalities/skrComputed'),
    valuesTrait: require('./modalities/skrValues'),
    conditionsTrait: require('./modalities/skrCondition'),
    defaultTrait: require('./modalities/skrDefault'),
    onlyWhenTrait: require('./modalities/skrOnlyWhen'),
    readOnlyTrait: require('./modalities/skrReadOnly'),
    requiredTrait: require('./modalities/skrRequired'),
    propertyNameTrait: require('./contentTypes/skrObject').propertyNameTrait,
    schemasTrait: require('./modalities/skrSchema'),
    mappingsTrait: require('./modalities/skrMapping'),
    labelTrait: require('./modalities/skrLabel'),
    helpTrait: require('./modalities/skrHelp'),
    stickyTrait: require('./modalities/skrSticky')
  },
  helpers: require('./core/helpers'),
  makeProfile: require('./core/makeProfile'),
  makeDocumentDataSource: require('./core/makeDocumentDataSource'),
  makeDocument: require('./core/makeDocument'),
  makeDescription: require('./core/makeDescription')
};
