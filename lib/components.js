/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');
var Case = require('case');

var componentTraits = {};
var groupTraits = {};

module.exports = {
  traits: componentTraits,
  groups: groupTraits,
  registerComponent: registerComponent,
  editorGroupTrait: editorGroupTrait,
  editorInputTrait: editorInputTrait
};

function registerComponent(component, env) {
  var group = env.skrEditorGroup;
  if (group) {
    component.group = group;
    group.addComponent(component);
  }
}

function makeEditorInput(description, options, componentName, contentType) {

  var self = {};

  // FIXME
  // The label and help text should move to the data descriptions.
  // For listing the validation messages, we copy it here directly
  if (!description.label) {
    description.label = options.label || options.name;
  }

  Object.defineProperties(self, {

    contentType: { value: contentType },
    description: { value: description },
    inputOptions: { value: options },
    componentName: { value: componentName },
    label: { value: options.label || options.name },
    help: { value: options.help },
    helpInLine: { value: options.helpInLine },

    // concrete input implementations should provide implementations for the
    // following methods
    nestedManipulator: {
      value: function () {
        throw new Error('function nestedManipulator not set');
      },
      writable: true
    },

    ensureIsOpen: {
      value: function () {
        self.group.ensureIsOpen();
      }
    }
  });

  return self;
}

function editorInputTrait(name, options) {
  options = options || {};
  var compileStep = {
    on: name,
    require: [ 'skrDescription' ],
    compile: function (env, config, skrDescription) {
      var contentType = this;
      var input = env.skrEditorComponent =
        makeEditorInput(skrDescription, config, name, contentType);
      registerComponent(env.skrEditorComponent, env);
      if (options.postCompile) {
        (options.postCompile)(env, input);
      }
    }
  };
  return {
    requires: [ 'field' ],
    contributes: [
      {
        name: 'compileSteps',
        value: compileStep
      }
    ]
  };
}

function editorInputDirective(name, options) {
  componentTraits[name] = editorInputTrait(name, options);
}

editorInputDirective('skrTextInput');
editorInputDirective('skrDateInput');
editorInputDirective('skrBooleanInput');
editorInputDirective('skrMultilineInput');
editorInputDirective('skrNumberInput');
editorInputDirective('skrChoiceInput', {
  postCompile: function (env, input) {
    skrOptionInputs(input, env);
  }
});
editorInputDirective('skrMultiChoiceInput', {
  postCompile: function (env, input) {
    skrOptionInputs(input, env);
  }
});
editorInputDirective('skrListInput', {
  postCompile: function (env, input) {
    env.skrEditorGroup = {
      addComponent: function (component) {
        if (input.listElementComponent) {
          throw new Error('Only one component allowed below skrListInput');
        }
        input.listElementComponent = component;
      }
    };
  }
});

// This function makes a given component behave like a group.
// Make sure the component has already been registered with its
// parent before calling this function.
function editorGroup(component, env) {
  var nestedComponents = [];

  env.skrEditorGroup = {
    addComponent: function (component) {
      nestedComponents.push(component);
    },
    ensureIsOpen: function () {
      component.ensureIsOpen();
    }
  };
  return {
    nestedComponents: nestedComponents
  };
}

function editorGroupTrait(on, register, options) {
  var compileStep = {
    on: on,
    compile: function (env, config) {

      options = options || {};

      var contentType = this;

      var component = env.skrEditorComponent = {
        contentType: contentType,
        description: env.skrDescription,
        componentName: options.componentName || 'skrEditorGroup',
        help: config.help,
        helpInLine: config.helpInLine,
        label: config.label || config.name,
        inputOptions: config,
        nestedManipulator: function (manipulator) {
          // a group within another group has no effect on the manipulator
          return manipulator;
        }
      };
      // register component before calling editorGroup()
      register = register || registerComponent;
      register(env.skrEditorComponent, env);

      // transfer the result of calling editorGroup() to component
      var group = editorGroup(env.skrEditorComponent, env);
      component.components = group.nestedComponents;

      // if this group is not nested in a section, it will serve as a
      // pseudo-section gathering top-level sections
      if (!env.skrSection) {
        var sections = env.skrEditorComponent.sections = [];

        env.skrSection = {
          depth: -1, // this way top-level sections get depth 0
          baseName: '',
          addSubSection: function (subsection) {
            sections.push(subsection);
            return [ sections.length ];
          },
          ensureIsOpen: function () {
            // nothing to do
          }
        };
      }

      component.ensureIsOpen = function () {
        env.skrSection.ensureIsOpen();
      };

      if (options.postCompile) {
        (options.postCompile)(env, component);
      }
    }
  };
  return {
    contributes: [
      {
        name: 'compileSteps',
        value: compileStep
      }
    ]
  };
}

function editorGroupOn(on, register, options) {
  groupTraits[on] = editorGroupTrait(on, register, options);
}

editorGroupOn('skrObject');

var skrPropertyNameCompileStep = {
  on: 'skrPropertyName',
  compile: function (env, node) {
    if (!node.name) { return; }
    var skrEditorComponent = env.skrEditorComponent;
    if (!skrEditorComponent) {
      throw new Error('skrPropertyName requires skrEditorComponent');
    }
    var propertyName = node.name;
    if (!env.hasOwnProperty('skrEditorComponent')) {
      throw new Error('skrPropertyName requires a sibling skrEditorComponent: ' + propertyName);
    }
    // we need to get the skrPropertyManipulator from the parent env in order to
    // avoid accidental overwrites with nested objects
    var skrPropertyManipulator = Object.getPrototypeOf(env).skrPropertyManipulator;

    if (!skrPropertyManipulator) {
      return;
    }
    skrEditorComponent.nestedManipulator = function (manipulator) {
      return skrPropertyManipulator(propertyName, manipulator);
    };
  }
};

componentTraits.propertyNameTrait = {
  requires: [ 'field' ],
  contributes: [
    {
      name: 'compileSteps',
      value: skrPropertyNameCompileStep
    }
  ]
};

var skrObjectCompileStep = {
  on: 'skrObject',
  compile: function (env) {
    env.skrPropertyManipulator = function (propertyName, manipulator) {
      return manipulator.propertyManipulators[propertyName];
    };
  }
};

componentTraits.skrObject = {
  contributes: [
    {
      name: 'compileSteps',
      value: skrObjectCompileStep
    }
  ]
};

var skrOptionInputCompileStep = {
  on: 'skrOptionInput',
  require: [ 'skrOption', 'skrOptionInputs' ],
  compile: function (env, config, skrOption, skrOptionInputs) {
    var optionName = skrOption.name;
    env.skrPropertyManipulator = function (propertyName, manipulator) {
      return manipulator
        .optionManipulators[optionName]
        .propertyManipulators[propertyName];
    };

    var optionInput = {
      name: optionName,
      label: config.label || config.name,
      help: config.help,
      helpInLine: config.helpInLine
    };
    skrOptionInputs.addOption(optionInput);
  }
};

componentTraits.skrOptionInput = {
  contributes: [
    {
      name: 'compileSteps',
      value: skrOptionInputCompileStep
    }
  ]
};

var skrSectionCompileStep = {
  on: 'skrSection',
  compile: function (env, node) {
    var contentType = this;
    var parentEnv = Object.getPrototypeOf(env);
    var parentSection = parentEnv.skrSection;
    var depth = parentSection ? parentSection.depth + 1 : 0;
    var subsections = [];
    var title = node.label || node.name;
    var baseName = (parentSection ? parentSection.baseName : '') +
      '_' + Case.snake(title);
    var section = env.skrEditorComponent = {
      contentType: contentType,
      description: env.skrDescription,
      componentName: 'skrSection',
      parentSection: parentEnv.skrSection,
      depth: depth,
      subsections: subsections,
      title: title,
      baseName: baseName,
      help: node.help,
      helpInLine: node.helpInLine,
      nestedManipulator: function (manipulator) {
        return manipulator;
      },
      ensureIsOpen: function () {
        if (parentSection) {
          parentSection.ensureIsOpen();
        }
        section.isOpen = true;
      }
    };
    // register section before calling editorGroup()
    registerComponent(section, env);
    var group = editorGroup(section, env);
    section.components = group.nestedComponents;

    // a section number is a list of numbers, e.g., [ 2, 3, 1 ]
    // the default (when no parent is found) is [ 1 ]
    var sectionNumber = [ 1 ];
    if (parentSection) {
      sectionNumber = parentSection.addSubSection(section);
    }
    section.sectionNumber = sectionNumber;
    section.name = 'section_' + Case.snake(sectionNumber) + section.baseName;

    section.isTopLevel = sectionNumber.length === 1;

    // If the document definition specifies whether the section should be open
    // or closed by default, we follow that. If there is no specification, we
    // automatically decide as follows: the first section of the document is
    // open by default, all the others are closed.
    var isOpenByDefault = node.isOpenByDefault;
    var autoIsOpen = section.isTopLevel && sectionNumber[0] === 1;
    section.isOpen = _.isBoolean(isOpenByDefault) ? isOpenByDefault : autoIsOpen;

    section.toggleOpen = function () {
      section.isOpen = !section.isOpen;
    };

    env.skrSection = {
      depth: depth,
      baseName: baseName,
      addSubSection: function (subsection) {
        subsections.push(subsection);
        return sectionNumber.concat([ subsections.length ]);
      },
      ensureIsOpen: function () {
        section.ensureIsOpen();
      }
    };
  }
};

groupTraits.skrSection = {
  contributes: [
    {
      name: 'compileSteps',
      value: skrSectionCompileStep
    }
  ]
};

function skrOptionInputs(input, env) {
  var options = input.options = [];
  var optionGroups = {};
  env.skrOptionInputs = {
    addOption: function (optionInput) {
      var optionName = optionInput.name;
      options.push(optionInput);
      Object.defineProperties(optionInput, {
        nestedComponent: {
          get: function () {
            return optionGroups[optionName];
          }
        }
      });
    },
    addOptionGroup: function (optionName, editorGroup) {
      optionGroups[optionName] = editorGroup;
    }
  };
}

editorGroupOn('skrOptionInput', function (editorGroup, env) {
  var optionName = env.skrOption.name;
  env.skrOptionInputs.addOptionGroup(optionName, editorGroup);
});

editorGroupOn('skrFieldSet', registerComponent, {
  componentName: 'skrFieldSet'
});
