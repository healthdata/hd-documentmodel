/* Copyright 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');

module.exports = processFlow;

function toposort(initial, next) {

  var visiting = {};
  var visited = {};
  var ordered = [];

  _.each(initial, visit);

  return ordered;

  function visit(name) {
    if (visiting[name]) {
      throw new Error('flow: circular steps with ' + name);
    }
    if (visited[name]) { return; }
    visiting[name] = true;
    _.each(next(name), visit);
    visiting[name] = false;
    visited[name] = true;
    ordered.push(name);
  }

}

function flow(steps, input, targets, cb) {

  // determine which steps to run and in what order
  var orderedSteps;
  try {
    orderedSteps = toposort(targets, stepPrerequisites);
  } catch (err) {
    process.nextTick(function () {
      cb(err);
    });
    return;
  }

  // initialize the state that will gather the results of each step
  var state = {};

  // combine the ordered steps in a single function
  var stateifiedSteps = _.map(orderedSteps, stateify);
  var combinedSteps = _.reduce(stateifiedSteps, sequenceSteps);

  // execute the combined steps and return the requested results to the
  // main callback
  combinedSteps(function (err) {
    if (err) { return cb(err); }
    cb(null, _.pick(state, targets));
  });

  // helper function to convert a step into a state transition function
  function stateify(stepName) {
    var step = steps[stepName];
    return function (cb) {
      // collect the prerequisites from the state
      var prerequisites = _.map(step.prerequisites, _.propertyOf(state));
      // invoke the step with the arguments, the main input, and a callback
      var args = prerequisites.concat([ input, callback ]);
      step.implementation.apply(null, args);
      function callback(err, result) {
        if (err) { return cb(err); }
        // store the result of the step in the state
        state[stepName] = result;
        cb();
      }
    };
  }

  // combine two state transition functions into one
  function sequenceSteps(stepsSoFar, step) {
    return function (cb) {
      stepsSoFar(function (err) {
        if (err) { return cb(err); }
        step(cb);
      });
    };
  }

  function stepPrerequisites(name) {
    var step = steps[name];
    if (!step) {
      throw new Error('flow: unknown step ' + name);
    }
    return step.prerequisites;
  }

}

function processFlow(actionTargets, defaultTargets, actions, input, targets, cb) {
  var err;
  // Check whether all actions are recognized.
  if (!_.all(actions, _.partial(_.contains, _.keys(actionTargets)))) {
    err = new Error('Unrecognized action');
    err.badRequest = true;
    return cb(err);
  }
  // Construct the set of recognized targets.
  var mappingTargets = defaultTargets();
  _.each(actions, function (action) {
    _.extend(mappingTargets, actionTargets[action]());
  });
  // Check whether the requested targets are recognized.
  if (!_.all(targets, _.partial(_.contains, _.keys(mappingTargets)))) {
    err = new Error('Unrecognized target');
    err.badRequest = true;
    return cb(err);
  }
  // Compute all requested targets, and return the results or report an error
  flow(mappingTargets, input, targets, cb);
}
