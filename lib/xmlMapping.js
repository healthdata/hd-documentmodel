/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');
var moment = require('moment');
var xmldom = require('xmldom').DOMParser;
var xpath = require('xpath');

var autocomplete = require('./autocomplete');

var xmlTraits = {};

module.exports = {
  traits: xmlTraits,
  importDocumentFromXML: importDocumentFromXML
};

/**
 * This is the boilerplate code to add XML support to an existing content type.
 * In the future we will probably have a more elegant solution for that, but
 * for now, using this function (and extending it when necessary) makes sure that
 * everything is consistent.
 */
function xmlImportDirective(options) {
  var builder = {
    on: options.on,
    require: options.require,
    build: function (skrDescription) {
      skrDescription.addManipulatorBuilder(function (manipulator) {
        manipulator.importBySchemaXML = function (schema, mapping, context) {
          // importBySchemaXML() should always return a context.
          // When the implementation does not return one, we reuse the original
          return (options.import)(manipulator, schema, mapping, context) || context;
        };
      });
    }
  };
  xmlTraits[options.on] = {
    contributes: [
      {
        name: 'descriptionBuilders',
        value: builder
      }
    ]
  };
}

/**
 * Helper function to extract some XML data using XPath.
 * @param  schema  The schema config being used.
 * @param  mapping The field-specific mapping config.
 * @param  suffix  An XPath suffix to be used.
 * @param  context The context contains, among others, the actual XML data.
 * @return         The result of the XPath query build from the parameters.
 */
function fetchXMLData(schema, mapping, suffix, context, nodesFunction) {
  var initialXPath = schema.initialXPath || '';
  var contextualXPath = context.contextualXPath || '';
  var mappingXPath = mapping.xpath || '';
  suffix = suffix || '';
  var combined = initialXPath + contextualXPath + mappingXPath + suffix;
  var query = nodesFunction ? nodesFunction + '(' + combined + ')' : combined;
  return context.xml(query);
}

/**
 * Helper function to extract a text value from a single XML node.
 * When there is not exactly one node, we return undefined.
 */
function singleText(xmlNodes) {
  return xmlNodes.length === 1 ? xmlNodes[0].nodeValue : undefined;
}

/**
 * The XML importer for text fields.
 */
xmlImportDirective({
  on: 'skrText',
  import: function (manipulator, schema, mapping, context) {
    var xmlNodes = fetchXMLData(schema, mapping, '/text()', context);
    var textValue = singleText(xmlNodes);
    if (textValue) {
      manipulator.value = textValue;
    }
  }
});

/**
 * The XML importer for number fields.
 */
xmlImportDirective({
  on: 'skrNumber',
  import: function (manipulator, schema, mapping, context) {
    var xmlNodes = fetchXMLData(schema, mapping, '/text()', context);
    var textValue = singleText(xmlNodes);
    if (textValue) {
      manipulator.value = parseFloat(textValue);
    }
  }
});

/**
 * The XML importer for boolean fields.
 */
xmlImportDirective({
  on: 'skrBoolean',
  import: function (manipulator, schema, mapping, context) {
    var xmlNodes = fetchXMLData(schema, mapping, '/text()', context);
    var textValue = singleText(xmlNodes);
    if (textValue === mapping.trueValue) { manipulator.value = true; }
    if (textValue === mapping.falseValue) { manipulator.value = false; }
  }
});

/**
 * The XML importer for date fields.
 */
xmlImportDirective({
  on: 'skrDate',
  import: function (manipulator, schema, mapping, context) {
    var xmlNodes = fetchXMLData(schema, mapping, '/text()', context);
    var textValue = singleText(xmlNodes);
    if (textValue) {
      var format = mapping.format || 'DD/MM/YYYY';
      manipulator.value = moment(textValue, format, true).toDate();
    }
  }
});

/**
 * The XML importer for list fields.
 */
xmlImportDirective({
  on: 'skrList',
  import: function (manipulator, schema, mapping, context) {
    var numberOfItems = fetchXMLData(schema, mapping, '', context, 'count');
    manipulator.setLength(numberOfItems);
    // this function should return an array of contexts to be used for each item in the list
    return _.map(_.range(numberOfItems), function (i) {
      var originalContextualXPath = context.contextualXPath || '';
      var positionSelector = '[position()=' + (i + 1) + ']'; // XPath counts from one, not zero.
      var newContextualXPath = originalContextualXPath + mapping.xpath + positionSelector;
      return _.extend({}, context, {
        contextualXPath: newContextualXPath
      });
    });
  }
});

/**
 * The main function to create a document based on an XML.
 * @param  document           The document to start with.
 * @param  schemaName         The name of the schema, as configured in the docdef.
 * @param  inputData          The XML data.
 * @param  cb                 The callback that will be invoked with the result.
 */
function importDocumentFromXML(document, schemaName, inputData) {
  /* jshint newcap: false */
  var xml = new xmldom().parseFromString(inputData);
  autocomplete.importBySchema(document, schemaName, {
    xml: function (q) {
      return xpath.select(q, xml);
    }
  });
}
