/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');

var traversalHelper = require('../traversalHelper');
var makeDescription = require('./makeDescription');
var makeDocumentDefinitionModel = require('./makeDocumentDefinitionModel');

module.exports = parseDocumentDefinition;

parseDocumentDefinition.makeDescriptionTrait = {
  provides: [ 'field' ],
  collects: [ 'descriptionBuilders' ],
  behaviour: {
    makeDescription: function (env, config) {
      var self = this;
      var description = makeDescription(self, env.__ddNode);
      _.each(this.descriptionBuilders, function (descriptionBuilder) {
        if (_.isFunction(descriptionBuilder)) {
          descriptionBuilder = {
            build: descriptionBuilder
          };
        }
        callWithRequired(descriptionBuilder.name, descriptionBuilder.require,
          self, [ description, config ], env, descriptionBuilder.build);
      });
      return description;
    }
  },
  contributes: [
    {
      name: 'compileSteps',
      value: function (env, config) {
        env.skrDescription = this.makeDescription(env, config);
      }
    }
  ]
};

function callWithRequired(name, require, self, defaultArgs, env, body, condition) {
  if (condition && !condition.apply(self, defaultArgs)) { return; }
  // resolve required environment variables
  var args = defaultArgs;
  _.map(require, function (req) {
    var value = env[req];
    if (!value) {
      throw new Error(name + ' requires ' + req);
    }
    args.push(value);
  });

  return body.apply(self, args);
}

parseDocumentDefinition.compileTrait = {
  provides: [ 'compile' ],
  collects: [ 'compileSteps' ],
  behaviour: {
    compile: function (env, config) {
      var self = this;
      return _.compact(_.map(this.compileSteps, function (compileStep) {
        if (_.isFunction(compileStep)) {
          compileStep = { compile: compileStep };
        }

        return callWithRequired(compileStep.on, compileStep.require,
          self, [ env, config ], env, compileStep.compile,
          compileStep.condition);
      }));
    }
  }
};

function compileNode(lookupNode, nodeRef, env) {
  var node = lookupNode(nodeRef);
  env.__ddNode = node.ddNodeModel;
  var postFns;
  if (node.resolved.$provides('compile')) {
    postFns = node.resolved.compile(env, node.input);
  }
  var nestedEnvs = _.mapObject(node.nested, function (nested) {
    return _.map(nested, function (nestedNode) {
      var nestedEnv = Object.create(env);
      compileNode(lookupNode, nestedNode, nestedEnv);
      return nestedEnv;
    });
  });
  _.each(postFns, function (postFn) { postFn(nestedEnvs); });
}

function parseDocumentDefinition(input, profile) {

  var nodeIDCounter = 0;
  var allNodes = [];

  var rootNode = traversalHelper(input, {
    initialContext: { parentID: null, typeContext: null },
    fold: fold,
    resolve: resolve,
    obtainNested: obtainNested
  });

  var model = makeDocumentDefinitionModel(allNodes, rootNode);

  function lookupNode(id) {
    var node = allNodes[id];
    var ddNodeModel = model.lookupById(id);
    return {
      ddNodeModel: ddNodeModel,
      resolved: node.resolved,
      input: node.input,
      nested: node.nested
    };
  }

  return {
    model: model,
    compile: function (env) {
      compileNode(lookupNode, rootNode, env);
    }
  };

  function newNodeID() {
    var result = nodeIDCounter;
    nodeIDCounter += 1;
    return result;
  }

  function resolve(input, context) {
    var pResolved = profile.resolve(input, context.typeContext);
    if (!pResolved) { return; }
    return {
      nodeID: newNodeID(),
      resolved: pResolved
    };
  }

  function obtainNested(resolved, input) {
    var hasNested = resolved.resolved.$provides('obtainNested');
    if (!hasNested) { return {}; }
    return _.mapObject(resolved.resolved.obtainNested(input), function (info) {
      return {
        nested: info.nested,
        context: {
          typeContext: info.context,
          parentID: resolved.nodeID
        }
      };
    });
  }

  function fold(intermediate) {
    var node = makeDocumentDefinitionNode(profile, intermediate);
    allNodes[node.nodeID] = node;
    return node.nodeID;
  }

}

function makeDocumentDefinitionNode(profile, intermediate) {
  if (intermediate.state !== traversalHelper.OK) {
    throw new Error('Unrecognized input in context "' + intermediate.context + '": ' +
      require('util').inspect(intermediate.input));
  }
  var resolved = intermediate.resolved.resolved;
  return {
    nodeID: intermediate.resolved.nodeID,
    parentID: intermediate.context.parentID,
    resolved: resolved,
    input: intermediate.input,
    nested: intermediate.nested
  };
}
