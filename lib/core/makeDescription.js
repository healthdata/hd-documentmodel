/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');
var util = require('../util');

module.exports = makeDescription;

function makeDescription(behaviour, ddNode) {
  var self = Object.create(makeDescription.prototype, {
    ddNode: {
      value: ddNode,
      enumerable: true
    },
    behaviour: {
      value: behaviour,
      enumerable: true
    },
    assertions: {
      value: {},
      enumerable: true
    },
    computedExpressions: {
      value: {},
      enumerable: true
    },
    nestedDescriptions: {
      value: {},
      enumerable: true
    },
    manipulatorBuilders: {
      value: [],
      enumerable: true
    },
    autoCompleters: {
      value: [],
      enumerable: true
    },
    absentPredicates: {
      value: [],
      enumerable: true
    },
    emptyPredicates: {
      value: [],
      enumerable: true
    },
    isActivePredicates: {
      value: [],
      enumerable: true
    },
    isReadyPredicates: {
      value: [],
      enumerable: true
    },
    messages: {
      value: {},
      enumerable: true
    },
    mappings: {
      value: {},
      enumerable: true
    },
    schemas: {
      value: {},
      enumerable: true
    }
  });
  self.absentPredicates.push(function (data) {
    return typeof data === 'undefined';
  });
  self.absentPredicates.push(function (data) {
    return data === null;
  });

  return self;
}

makeDescription.prototype = {
  isEmpty: function isEmpty(data, manipulator) {
    return util.anyPredicate(this.emptyPredicates, data, manipulator);
  },
  isAbsent: function isAbsent(data, manipulator) {
    return util.anyPredicate(this.absentPredicates, data, manipulator);
  },
  setDefaultValue: function (value) {
    this.defaultValue = value;
  },
  testAssertions: function testAssertions(data, manipulator) {
    return _.object(_.map(this.assertions, function (assertion, name) {
      return [ name, assertion.check(data, manipulator) ];
    }));
  },
  buildManipulator: function (manipulator) {
    _.each(this.manipulatorBuilders, function (builder) {
      builder(manipulator);
    });
  },
  addManipulatorBuilder: function (builder) {
    this.manipulatorBuilders.push(builder);
  },
  messageFor: function (tag) {
    return this.messages[tag];
  },
  addMapping: function (mappingConfig) {
    // TODO only mappings for known schemas schould be allowed
    var name = mappingConfig.name;
    if (typeof name !== 'string' || name === '' || _.has(this.mappings, name)) {
      throw new Error('Invalid or missing name for mapping ', mappingConfig);
    }
    this.mappings[name] = mappingConfig;
  },
  addSchema: function (schemaConfig) {
    var name = schemaConfig.name;
    if (typeof name !== 'string' || name === '' || _.has(this.schemas, name)) {
      throw new Error('Invalid or missing name for schema ', schemaConfig);
    }
    this.schemas[name] = schemaConfig;
  }
};
