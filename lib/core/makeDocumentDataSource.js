/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');

module.exports = makeDocumentDataSource;

/**
 * A document data source object provides an interface to the 'raw' data unerlying a document.
 * Currently, this is the only implementation, but in the future we may add other data source
 * that interact in real time with a server. By adding this abstraction, we keep the details
 * of storing and fetching document data out of the other objects such as Document and Manipulator.
 * @return {[type]} [description]
 */
function makeDocumentDataSource(documentData, options) {

  options = options || {};
  var pathPrefix = options.pathPrefix || [ 'value' ];

  return {
    accessPath: accessPath,
    metadataFor: metadataFor
  };

  // Only functions below

  function readJSONPath(jsonPath) {
    return _.reduce(jsonPath, step, documentData);
    function step(memo, propertyName) {
      return memo[propertyName];
    }
  }

  function writeJSONPath(jsonPath, value) {
    _.reduce(jsonPath, step, documentData);
    function step(memo, propertyName, index, propertyNames) {
      if (index + 1 === propertyNames.length) {
        memo[propertyName] = value;
        return;
      }
      return memo[propertyName];
    }
  }

  function accessPath(path) {
    var jsonPath = pathPrefix.concat(path);
    return {
      get: function () { return readJSONPath(jsonPath); },
      set: function (value) { writeJSONPath(jsonPath, value); }
    };
  }

  function pathAsString(path) {
    return _.reduce(path, function (result, propertyName) {
      return result + '.' + propertyName;
    }, '%');
  }

  function metadataFor(name, path) {
    var key = pathAsString(path);
    lookupMetaData.set = setMetaData;
    return lookupMetaData;
    function lookupMetaData(forceExists) {
      if (!documentData[name]) {
        if (forceExists) {
          documentData[name] = {};
        } else {
          return undefined;
        }
      }
      if (typeof documentData[name][key] === 'undefined') {
        if (forceExists) {
          documentData[name][key] = forceExists();
        } else {
          return undefined;
        }
      }
      return documentData[name][key];
    }
    function setMetaData(v) {
      if (!documentData[name]) {
          documentData[name] = {};
      }
      documentData[name][key] = v;
    }
  }

}
