/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');

module.exports = {
  defaultComputedExpressionArguments: defaultComputedExpressionArguments,
  setLabelAndHelp: setLabelAndHelp,
  defineComputedExpression: defineComputedExpression,
  defineCondition: defineCondition,
  wrapReinitialization: wrapReinitialization,
  setupSimpleDefaultValue: setupSimpleDefaultValue,
  propertyManipulators: propertyManipulators,
  clearManipulators: clearManipulators,
  clearInactiveManipulators: clearInactiveManipulators,
  wrapExpression: wrapExpression,
  simpleDefaultValue: simpleDefaultValue,
  simpleValue: simpleValue,
  defineSimpleValueProperty: defineSimpleValueProperty
};

function defineCondition(skrDescription, name, expression, message, level) {
  var condition = wrapExpression(expression);

  skrDescription.assertions[name] = {
    level: level === 'warning' ? 'warning' : undefined,
    check: function checkCondition(data, manipulator) {
      try {
        return condition(manipulator) ? 'ok' : 'failed';
      } catch (err) {
        // A condition that failed to be computed is treated as a failed validation.
        return 'failed';
      }
    }
  };

  skrDescription.messages[name] = message;
}

function defineComputedExpression(skrDescription, name, expression) {
  var compute = wrapExpression(expression);

  skrDescription.computedExpressions[name] = {
    compute: function (data, manipulator) {
      try {
        return compute(manipulator);
      } catch (err) {
        // If computing the result failed, we simply return undefined.
        // Logging the error is not ideal, because during the lifetime of
        // a document there may be many computed expressions that fail.
        return undefined;
      }
    }
  };
}

function defaultComputedExpressionArguments() {
  return {
    '$': function (manipulator) { return manipulator.read(); },
    '$$': function (manipulator) { return manipulator; }
  };
}

function defineSimpleValueProperty(manipulator) {
  Object.defineProperty(manipulator, 'value', {
    get: function () { return manipulator.read(); },
    set: function (v) { manipulator.edit(function () { return v; }); }
  });
}

function wrapReinitialization(manipulator, f) {
  var original = manipulator.reinitialize.bind(manipulator);
  manipulator.reinitialize = function () {
    original();
    f();
  };
}

function setupSimpleDefaultValue(description, manipulator) {
  function doIt() {
    // If the whole document is read-only, we do nothing;
    // if only this manipulator is read-only, we override.
    if (!manipulator.document.readOnly) {
      var hasDefaultValue = description.behaviour.hasDefaultValue &&
        description.behaviour.hasDefaultValue(description);
      if (hasDefaultValue) {
        manipulator.document.inExtendedEditContext('defaultValue', function () {
          manipulator.initialize(
            description.behaviour.defaultValue(manipulator),
            true);
        });
      }
    }
  }
  wrapReinitialization(manipulator, doIt);
  doIt();
}

function propertyManipulators(propertyDescriptions, parent, isActive) {
  var result = {};
  _.each(propertyDescriptions, function (description, propertyName) {
    result[propertyName] = parent.propertyManipulator(propertyName, description, isActive);
  });
  return result;
}

function clearManipulators(manipulators) {
  _.each(manipulators, function (manipulator) {
    manipulator.clear();
  });
}

function clearInactiveManipulators(manipulators) {
  _.each(manipulators, function (manipulator) {
    manipulator.isActive();
  });
}

function wrapExpression(expression) {
  /* jshint evil: true */
  var code = '"use strict"; return (' + expression + ')';
  return function (manipulator) {
    var computedExpressionArguments = manipulator.document.computedExpressionArguments;
    var exposedVariables = _.keys(computedExpressionArguments);
    var concreteArgs = _.map(exposedVariables, function (variableName) {
      return computedExpressionArguments[variableName](manipulator);
    });
    var compute = Function.apply(null, exposedVariables.concat(code));
    return compute.apply(null, concreteArgs);
  };
}

function simpleDefaultValue(description) {
  description.addManipulatorBuilder(
    function (manipulator) {
      setupSimpleDefaultValue(description, manipulator);
    });
}

function simpleValue(description) {
  description.addManipulatorBuilder(defineSimpleValueProperty);
  simpleDefaultValue(description);
}

function setLabelAndHelp(on, config) {
  on.label = config.label || config.name;
  on.help = config.help;
}
