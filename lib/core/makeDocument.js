/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');
var memoized = require('../memoized');
var makeManipulator = require('./makeManipulator');
var helpers = require('./helpers');

module.exports = makeDocument;

function pathAsString(path) {
  return _.reduce(path, function (result, propertyName) {
    return result + '.' + propertyName;
  }, '%');
}

function makeDocument(documentDataSource, description, options) {
  var self = {};
  var registeredManipulators = {};

  var memoizedGroup = memoized.group();

  function computeErrors() {
    var errors = [];
    _.each(registeredManipulators, function (manipulator) {
      if (manipulator.isActive()) {
        _.each(manipulator.errors, function (error) {
          errors.push({
            manipulator: manipulator,
            assertion: error
          });
        });
      }
    });
    return errors;
  }

  function computeWarnings() {
    var warnings = [];
    _.each(registeredManipulators, function (manipulator) {
      if (manipulator.isActive()) {
        _.each(manipulator.warnings, function (warning) {
          warnings.push({
            manipulator: manipulator,
            assertion: warning
          });
        });
      }
    });
    return warnings;
  }

  function computeProgress() {
    var progress = {
      valid: 0,
      invalid: 0,
      optional: 0
    };
    _.each(registeredManipulators, function (manipulator) {
      if (manipulator.isActive()) {
        if (manipulator.errors.length === 0) {
          if (manipulator.countAsField) {
            var value = manipulator.read();
            var description = manipulator.description;
            var absentOrEmpty = description.isAbsent(value, manipulator) ||
                                description.isEmpty(value, manipulator);
            if (absentOrEmpty) {
              progress.optional += 1;
            } else {
              progress.valid += 1;
            }
          }
        } else {
          progress.invalid += 1;
        }
      }
    });
    progress.total = progress.valid +
                     progress.invalid +
                     progress.optional;
    progress.validRatio = progress.valid / progress.total;
    progress.invalidRatio = progress.invalid / progress.total;
    progress.optionalRatio = progress.optional / progress.total;
    return progress;
  }

  function computeComments() {
    var comments = [];
    _.each(registeredManipulators, function (manipulator) {
      _.each(manipulator.comments, function (comment) {
        comments.push({
          manipulator: manipulator,
          comment: comment
        });
      });
    });
    return comments;
  }

  options = options || {};
  if (options.clearInactive !== false) {
    options.clearInactive = true;
  }

  var computedExpressionArguments = options.computedExpressionArguments ||
    helpers.defaultComputedExpressionArguments();

  var changeListeners = [];

  Object.defineProperties(self, {
    document: { value: self },
    options: { value: options },

    readOnly: { value:  Boolean(options.readOnly) },

    editContext: {
      writable: true,
      value: options.initialEditContext || [ 'edit' ]
    },

    computedExpressionArguments: { value: computedExpressionArguments },

    registeredManipulators: { value: registeredManipulators },

    isActive: { value: function () {
      return true;
    }},

    isDirty: { value: false, writable: true },

    computedExpressions: { value: {} },
    computedExpressionsMemoized: { value: {} },

    listen: { value: function (listener) {
      changeListeners.push(listener);
      return unsubscribe;
      function unsubscribe() {
        var pos = changeListeners.indexOf(listener);
        changeListeners.splice(pos, 1);
      }
    }},

    changed: { value: function () {
      _.each(changeListeners, function (listener) {
        listener();
      });
    }},

    isReady: { value: function () {
      return _.all(registeredManipulators, function (manipulator) {
        return manipulator.isReady();
      });
    }},

    compilePath: { value: function (path, config) {
      return documentDataSource.accessPath(path, config);
    }},

    metadataFor: { value: function (name, path) {
      return documentDataSource.metadataFor(name, path);
    }},

    inExtendedEditContext: { value: function (context, body) {
      var oldContext = self.editContext;
      self.editContext = (oldContext || []).concat([ context ]);
      try {
        return body();
      } finally {
        self.editContext = oldContext;
      }
    }},

    errors: memoizedGroup.memoized(computeErrors).propertyDescriptor(),
    warnings: memoizedGroup.memoized(computeWarnings).propertyDescriptor(),
    progress: memoizedGroup.memoized(computeProgress).propertyDescriptor(),
    comments: memoizedGroup.memoized(computeComments).propertyDescriptor(),

    traverseWithContext: { value: function (initialContext, f) {
      traverseLoop(self.root, initialContext);
      function traverseLoop(manipulator, context) {
        var nestedContexts = f(manipulator, context);
        _.each(manipulator.nestedManipulators, function (nestedManipulator, position) {
          var nestedContext = nestedContexts ? nestedContexts[position] : context;
          traverseLoop(nestedManipulator, nestedContext);
        });
      }
    }},

    registerManipulator: { value: function (manipulator) {
      var path = manipulator.path;
      var key = pathAsString(path);

      if (registeredManipulators[key]) {
        throw new Error('Path already in use: ' + key);
      }
      registeredManipulators[key] = manipulator;
    }},

    invalidate: { value: function () {
      // There should always be a single invalidation "process" running, so recursive calls
      // should be intercepted.
      // During the INVALIDATING phase, we simply do not allow calls to invalidate(),
      // during the POST_INVALIDATION phase, we note that invalidation has been requested,
      // causing the invalidation "process" to continue interation.
      if (self.invalidationState === 'INVALIDATING') {
        throw new Error('invalidation should never trigger more invalidation');
      }
      if (self.invalidationState === 'POST_INVALIDATION') {
        self.continueInvalidationProcess = true;
        return;
      }
      // Here we start the actual invalidation process.
      self.continueInvalidationProcess = true;
      var count = 0;
      while (self.continueInvalidationProcess) {
        count += 1;
        if (count >= 100) {
          throw new Error('detected a loop during invalidation');
        }
        self.invalidationState = 'INVALIDATING';
        memoizedGroup.invalidate();
        _.each(registeredManipulators, function (manipulator) {
          manipulator.invalidateLocal();
        });
        // We set continueInvalidationProcess to false, hoping that this is the last iteration.
        // If during the calls to manipulator.postInvalidate() further invalidation is requested,
        // continuePostInvalidation will be set to true, and the process will continue.
        self.continueInvalidationProcess = false;
        self.invalidationState = 'POST_INVALIDATION';
        _.each(registeredManipulators, function (manipulator) {
          manipulator.postInvalidate();
        });
      }
      self.invalidationState = null;
      self.isDirty = true;
      self.changed();
    }},

    deregisterManipulator: { value: function (manipulator) {
      var path = manipulator.path;
      var key = pathAsString(path);
      delete registeredManipulators[key];
    }}
  });

  Object.defineProperties(self, {
    root: { value: makeManipulator(self, [], [], description) }
  });

  return self;
}
