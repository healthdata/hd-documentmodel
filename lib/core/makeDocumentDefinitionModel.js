/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');

module.exports = makeDocumentDefinitionModel;

function makeDocumentDefinitionModel(internalNodes, rootNodeID) {

  var self, ddNodes, rootNode;

  self = {
    root: function () { return rootNode; },
    lookupById: function (id) { return ddNodes[id]; }
  };

  ddNodes = _.map(internalNodes, _.partial(makeDDNode, _, self));
  rootNode = ddNodes[rootNodeID];

  return self;

}

function makeDDNode(internalNode, documentDefinitionModel) {
  var nodeTypeImpl = internalNode.resolved;

  var self = {
    documentDefinition: function () { return documentDefinitionModel; }
  };

  // Install all methods provided by the node type implementation.

  var methodNames = nodeTypeImpl.$methodsOf('DDNode');
  _.each(methodNames, function (methodName) {
    if (_.has(self, methodName)) {
      throw new Error('makeDDNode: duplicate method name ' + methodName);
    }
    self[methodName] = _.partial(nodeTypeImpl[methodName], self);
  });

  // Install all properties provided by the node type implementation, using
  // memoization to cache the values.

  var propertyNames = nodeTypeImpl.$propertiesOf('DDNode');
  _.each(propertyNames, function (info) {
    if (_.has(self, info.name)) {
      throw new Error('makeDDNode: duplicate method name ' + info.name);
    }
    var memoized = makeMemoized(_.partial(nodeTypeImpl[info.member], self));
    self[info.name] = function () {
      return readMemoized(memoized);
    };
  });

  return self;
}

function makeMemoized(compute) {
  return {
    isBusy: false,
    isDone: false,
    compute: compute,
    value: undefined
  };
}

function readMemoized(memoized) {
  if (memoized.isBusy) {
    throw new Error('memoized: circular dependency');
  }
  if (memoized.isDone) {
    return memoized.value;
  }
  memoized.isBusy = true;
  var result = memoized.value = (memoized.compute)();
  memoized.isBusy = false;
  memoized.isDone = true;
  memoized.compute = null;
  return result;
}
