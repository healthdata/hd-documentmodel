/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');
var traits = require('../traits');

module.exports = makeProfile;

function makeProfile(config) {

  // This oneliner is where the magic happens: for each type provided in the config,
  // we combine all the given traits and apply as much modalities as possible. This
  // gives us "wrapped up" implementations for each type.
  var implementations = _.mapObject(config.types, _.partial(traits.wrap, _, config.modalities));

  return {
    resolve: _.compose(
      _.partial(lookupImplementation, implementations),
      _.partial(resolve, config.contexts, config.defaultContext))
  };
}

function lookupImplementation(implementations, resolvedType) {
  return _.isString(resolvedType) ? implementations[resolvedType] : null;
}

function resolve(contexts, defaultContext, input, context) {
  if (!_.isObject(input)) {
    return;
  }
  if (!_.isString(context)) {
    context = defaultContext;
  }
  if (!_.isString(context)) {
    return;
  }
  if (!_.has(contexts, context)) {
    return;
  }
  var contextInfo = contexts[context];
  if (contextInfo.overrides) {
    var matches = _.intersection(_.keys(input), _.keys(contextInfo.overrides));
    if (matches.length) {
      // We _arbitrarily_ choose the first match.
      return contextInfo.overrides[matches[0]];
    }
  }
  var type = input.type;
  var hasType = _.isString(type);
  if (!hasType && contextInfo.defaultType) {
    return contextInfo.defaultType;
  }
  if (hasType && contextInfo.types && contextInfo.types[type]) {
    return contextInfo.types[type];
  }
  // could not resolve
  return;
}
