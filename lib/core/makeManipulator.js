/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');
var util = require('../util');
var memoized = require('../memoized');

module.exports = makeManipulator;

function Manipulator() {}

function makeManipulator(parent, path, pathConfig, description, isActive) {

  isActive = isActive || function () { return true; };

  var options = parent.options || {};
  var document = parent.document;

  var self = new Manipulator();

  var postInvalidateListeners = [];

  var memoizedGroup = memoized.group();

  function computeAssertions() {
    return self.testAssertions();
  }

  var fieldPathConfig = description.pathConfig && description.pathConfig(path);

  pathConfig = fieldPathConfig ?
    pathConfig.concat([ fieldPathConfig ]) : pathConfig;

  var target = document.compilePath(path, pathConfig);

  var getComments = document.metadataFor('comments', path);
  var getIgnoredWarnings = document.metadataFor('ignoredWarnings', path);

  var errorsAndWarnings = memoizedGroup.memoized(
    function computeErrorsAndWarnings() {
      var errors = [];
      var warnings = [];
      _.each(self.assertions, function (status, name) {
        if (status === 'failed') {
          var isWarning = description.assertions[name].level === 'warning';
          if (isWarning && _.contains(getIgnoredWarnings(), name)) { return; }
          (isWarning ? warnings : errors).push(name);
        }
      });
      return { errors: errors, warnings: warnings };
    });

  function computeErrors() {
    return errorsAndWarnings.value().errors;
  }

  function computeWarnings() {
    return errorsAndWarnings.value().warnings;
  }

  // for easy lookup, we let the computed expression objects inherit
  // from the parent's
  // time will tell whether this is a good idea or not
  var computedExpressionsMemoized = Object.create(parent.computedExpressionsMemoized);
  var computedExpressions = Object.create(parent.computedExpressions);
  _.each(description.computedExpressions, function (ce, name) {
    function compute() {
      var data = self.read();
      return ce.compute(data, self);
    }
    var memoized = computedExpressionsMemoized[name] = memoizedGroup.memoized(compute);
    Object.defineProperty(computedExpressions, name,
      memoized.propertyDescriptor());
  });

  self.document = document;
  self.parent = parent;
  self.description = description;
  self.options = options;
  self.readOnly = Boolean(options.readOnly);
  self.path = path;
  self.countAsField = true;
  self.computedExpressions = computedExpressions;
  self.computedExpressionsMemoized = computedExpressionsMemoized;

  self.computedExpression = function (name) {
    return computedExpressions[name];
  };

  self.addComment = function addComment(comment) {
    var comments = getComments(function () { return []; });
    comments.push(comment);
    self.invalidate();
  };

  self.deleteComment = function (position) {
    var comments = getComments(function () { return []; });
    comments.splice(position, 1);
    self.invalidate();
  };

  self.propertyManipulator = function (propertyName, description, isActive, config) {
    var pPath = path.concat([ propertyName ]);
    var pConfig = config ? pathConfig.concat([ config ]) : pathConfig;
    return makeManipulator(self, pPath, pConfig, description, isActive);
  };

  self.isActive = function () {
    var parentIsActive = parent.isActive();
    var result = parentIsActive &&
                 isActive() &&
                 util.allPredicates(description.isActivePredicates, self);
    return result;
  };

  self.read = function read() {
    return target.get();
  };

  self.edit = function edit(f, overrideReadOnly) {
    if (!self.readOnly || overrideReadOnly) {
      target.set(f(target.get()));
      self.editContext = document.editContext;
      self.invalidate();
    }
  };

  self.invalidate = function () {
    self.document.invalidate();
  };

  self.invalidateLocal = function () {
    memoizedGroup.invalidate();
  };

  self.onPostInvalidate = function (listener) {
    postInvalidateListeners.push(listener);
  };

  self.postInvalidate = function () {
    _.each(postInvalidateListeners, function (postInvalidateListener) {
      postInvalidateListener();
    });
  };

  self.initialize = function initialize(value, overrideReadOnly) {
    document.inExtendedEditContext('initialize', function () {
      self.edit(function (original) {
        if (typeof original === 'undefined') {
          return value;
        }
        return original;
      }, overrideReadOnly);
    });
  };

  self.reinitialize = function () {};

  self.setDefaultValue = function (value, overrideReadOnly) {
    self.initialize(value, overrideReadOnly);
  };

  self.clear = function clear() {
    document.inExtendedEditContext('clear', function () {
      if (self.clearSpecific) {
        self.clearSpecific();
      } else {
        // only edit when needed
        if (!_.isUndefined(self.read())) {
          self.edit(function () {
            return undefined;
          });
        }
      }
    });
  };

  self.autoCompleteWith = function (inputData) {
    // invoke the local auto completers
    var autoCompleters = self.description.autoCompleters;
    var affected = false;
    _.each(autoCompleters, function (autoCompleter) {
      affected = autoCompleter.autocomplete(self, inputData) || affected;
    });
    if (self.csvImportedByParent) {
      affected = true;
    }
    var result = affected ? [ self ] : [];
    // descend to nested manipulators
    var nested = _.map(self.nestedManipulators, function (nestedManipulator) {
      return nestedManipulator.autoCompleteWith(inputData);
    });
    return result.concat(_.flatten(nested));
  };

  self.importBySchema = function (schema, mapping, context) {
    // TODO this is not quite right ...
    // we should not assume the supported formats
    if (schema.format === 'xml') {
      if (!self.importBySchemaXML) {
        throw new Error('Cannot import XML for content type ' + self.description.type);
      }
      return self.importBySchemaXML(schema, mapping, context);
    }
    return context;
  };

  self.testAssertions = function () {
    return self.description.testAssertions(self.read(), self);
  };

  self.ignoreWarning = function (assertionName) {
    var ignoredWarnings = getIgnoredWarnings(function () { return []; });
    ignoredWarnings.push(assertionName);
    // TODO invalidating all memoized values is not necessary
    memoizedGroup.invalidate();
  };

  self.destroy = function () {
    document.deregisterManipulator(self);
    _.each(self.nestedManipulators, function (manipulator) {
      manipulator.destroy();
    });
  };

  self.isReady = function () {
    return util.allPredicates(description.isReadyPredicates, self);
  };

  /**
   * This method is responsible for dealing with computed values. Each content type
   * can provide a custom implementation if the default is not sufficient.
   */
  self.acceptComputedValue = function (newValue) {
    self.edit(function () { return newValue; }, true);
  };

  Object.defineProperties(self, {
    comments: { get: function () { return getComments(); }},
    assertions: memoizedGroup.memoized(computeAssertions).propertyDescriptor(),
    errors: memoizedGroup.memoized(computeErrors).propertyDescriptor(),
    warnings: memoizedGroup.memoized(computeWarnings).propertyDescriptor()
  });

  document.registerManipulator(self);
  description.buildManipulator(self);

  // The intialization logic in the manipulator builders did not have the
  // correct value of isActive() yet. Therefore, we clear the manipulator if
  // required.
  if (self.options.clearInactive && !self.isActive() && parent.isActive()) {
    self.clear();
  }

  self.wasActive = self.isActive();
  self.onPostInvalidate(function () {
    var result = self.isActive();
    // Check whether there is a change in activity
    // if so, trigger proper action: clear when becoming inactive
    // reinitialize when becoming active
    if (self.wasActive === result) { return; }
    self.wasActive = result;
    if (result) {
      // we just became active, so reinit
      self.reinitialize();
    } else {
      // we just became inactive, so clear
      // only when this behavior is preferred
      if (self.options.clearInactive && parent.isActive()) {
        self.clear();
      }
    }
  });

  return self;
}
