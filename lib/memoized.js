/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');

var memoized = module.exports = function memoized(compute) {

  var memoizedValue;
  var shouldRecompute = true;

  var isRecomputing = false;

  var self;

  return (self = {

    value: function () {

      if (isRecomputing) {
        throw new Error('memoized: circular dependency');
      }

      if (shouldRecompute) {
        shouldRecompute = false;
        isRecomputing = true;
        memoizedValue = compute();
        isRecomputing = false;
      }
      return memoizedValue;
    },

    propertyDescriptor: function () {
      return { get: self.value, enumerable: true };
    },

    invalidate: function () {
      shouldRecompute = true;
      memoizedValue = undefined;
    }
  });

};

memoized.group = function () {
  var ms = [];
  var self;
  return (self = {
    memoized: function (compute) {
      var m = memoized(compute);
      ms.push(m);
      return m;
    },
    invalidate: function () {
      _.each(ms, function (m) {
        m.invalidate();
      });
    }
  });
};
