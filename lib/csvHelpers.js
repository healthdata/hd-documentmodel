/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');

var traits = require('./traits');
var autocompleteTrait = require('./autocomplete').autocompleteTrait;

module.exports = {
  csvData: csvData,
  pathAsString: pathAsString,
  simpleValueCSV: simpleValueCSV,
  simpleValueBuildCSV: simpleValueBuildCSV,
  simpleValueAutocomplete: simpleValueAutocomplete,
  join: join,
  split: split,
  makeCSVBuilder: makeCSVBuilder,
  exportDocumentToCSV: exportDocumentToCSV
};

// Use a given string as regular expression matching just that string.
// To do this safely, special characters in the original string must be escaped.
function asRegExp(str, flags) {
  // http://stackoverflow.com/questions/3446170/escape-string-for-use-in-javascript-regex
  // http://stackoverflow.com/questions/3561493/is-there-a-regexp-escape-function-in-javascript
  return new RegExp(str.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&'), flags);
}

// Pick a suitable character to escape a separator.
// We prefer backslash, but this cannot be used when the separator itself is a
// backslash. In that case, we use a vertical bar.
function escapeChar(separator) {
  return separator === '\\' ? '|' : '\\';
}

function escape(value, separator) {
  if (typeof value !== 'string') {
    return value;
  }
  var esc = escapeChar(separator);
  var escRE = asRegExp(esc, 'g');
  var sepRE = asRegExp(separator, 'g');
  return value
    .replace(escRE, esc + esc)
    .replace(sepRE, esc + separator);
}

function unescape(escapedValue, separator) {
  var esc = escapeChar(separator);
  var escRE = asRegExp(esc + esc, 'g');
  var sepRE = asRegExp(esc + separator, 'g');
  return escapedValue
    .replace(sepRE, separator)
    .replace(escRE, esc);
}

// Join an array of values with a separator, escaping occurrences of the
// separator itself
function join(values, separator) {
  return _.map(values, function (value) {
    return escape(value, separator);
  }).join(separator);
}

// Split a string based on a separator, but do not split where the separator
// has been escaped.
function split(values, separator) {
  var esc = escapeChar(separator);
  // There is no way the tell JavaScript's split to ignore escaped separators,
  // so we first split on the escaped separators, then split on actual separators,
  // reorder the nested arrays, then join the pieces of the first split back toghether.
  var escSep = esc + separator;
  var doublySplit = _.map(values.split(escSep), function (piece) {
    return piece.split(separator);
  });
  var reordered = [[]];
  _.each(doublySplit, function (arr) {
    _.each(arr, function (value, position) {
      // the first item in the array was only separated with an escaped
      // separator so belongs the last element of last array in the reslt.
      if (position !== 0) {
        reordered.push([]);
      }
      var last = reordered[reordered.length - 1];
      last.push(value);
    });
  });
  return _.map(reordered, function (values) {
    return unescape(values.join(escSep), separator);
  });
}

function pathAsString(path) {
  return path.join('|');
}

function csvData(inputData, manipulator, suffix) {
  suffix = suffix || '';
  var key = pathAsString(manipulator.path) + suffix;
  return inputData.csv[key];
}

function simpleValueBuildCSV(description) {
  description.buildCSV = function (manipulator, csvBuilder) {
    var key = pathAsString(manipulator.path);
    var value = manipulator.exportToCSV();
    csvBuilder.addColumn(key, value);
  };
}

function simpleValueAutocomplete(manipulator, inputData, config) {
  /* jshint unused: false */
  if (!inputData.csv) { return; }
  var result = csvData(inputData, manipulator);
  if (typeof result !== 'string') { return; }
  return manipulator.importFromCSV(result);
}

function simpleValueCSV(config) {
  function build(skrDescription) {
    skrDescription.isSimpleValue = true;
    simpleValueBuildCSV(skrDescription);

    skrDescription.addManipulatorBuilder(function (manipulator) {
      manipulator.importFromCSV = function (value) {
        return config.importFromCSV(manipulator, value);
      };
      manipulator.exportToCSV = function () {
        return config.exportToCSV(manipulator);
      };
    });
  }
  var simpleValueTrait = {
    contributes: [
      {
        name: 'descriptionBuilders',
        value: build
      }
    ]
  };
  var acTrait = autocompleteTrait({
    on: config.on,
    autocomplete: simpleValueAutocomplete
  });
  return traits.compose(simpleValueTrait, acTrait);
}

function makeCSVBuilder() {
  var numberOfColumns = 0;
  var columnNames = [];
  var columnPositions = {};
  var rows = [];
  var values = [];
  return {
    addColumn: function (columnName, value) {
      var position;
      if (_.has(columnPositions, columnName)) {
        position = columnPositions[columnName];
      } else {
        position = numberOfColumns;
        columnNames[position] = columnName;
        columnPositions[columnName] = position;
        numberOfColumns += 1;
      }
      values[position] = value;
    },
    toProtoCSV: function () {
      // make the size of each row match the number of columns
      _.each(rows, function (row) {
        _.times(columnNames.length - row.length, function () {
          row.push(undefined);
        });
      });
      // return csv.stringify([ columnNames ].concat(rows), { delimiter: ';' }, cb);
      return {
        columns: columnNames,
        rows: rows
      };
    },
    endOfLine: function () {
      rows.push(values);
      values = [];
    }
  };
}

function exportDocumentToCSV(document, csvBuilder, options) {
  options = options || {};
  if (typeof document === 'function') {
    document = document();
  }
  _.each(document.registeredManipulators, function (manipulator) {
    if (!manipulator.isActive()) { return; }
    if (manipulator.description.skipCSVExport) { return; }
    if (options.skip && options.skip(manipulator)) { return; }
    var buildCSV = manipulator.description.buildCSV;
    if (typeof buildCSV === 'function') {
      buildCSV(manipulator, csvBuilder);
    }
  });
  csvBuilder.endOfLine();
}
