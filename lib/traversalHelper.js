/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');

module.exports = traversalHelper;

var UNRESOLVED      = traversalHelper.UNRESOLVED      = 'UNRESOLVED';
var OK              = traversalHelper.OK              = 'OK';

function traversalHelper(input, params) {
  var initialContext   = params.initialContext;
  var fold             = params.fold || _.identity;
  var resolve          = params.resolve;
  var obtainNested     = params.obtainNested;

  return loop(input, initialContext);

  function loop(inputPart, context) {

    var resolved = resolve(inputPart, context);

    if (!resolved) {
      return fold({
        state: UNRESOLVED,
        context: context,
        input: inputPart
      });
    }

    // Loop over nested entities. If `nestedInput` contains for example
    // `{ foo: { context: 'gunk', nested: [ ...] } }`, then we process the elements of
    // `nestedInput.foo.nested` using context `'gunk'`. The result will have the following
    // structure: `{ foo: [ ... ] }`
    var nestedInput = obtainNested(resolved, inputPart);
    var nestedResults = _.mapObject(nestedInput, function (info) {
      var nestedContext = info.context;
      return _.map(info.nested, function (nestedInputPart) {
        return loop(nestedInputPart, nestedContext);
      });
    });

    return fold({
      state: OK,
      input: inputPart,
      context: context,
      resolved: resolved,
      nested: nestedResults
    });
  }
}
