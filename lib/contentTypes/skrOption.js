/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var helpers = require('../core/helpers');

var optionTrait = {
  provides: [ 'obtainNested' ],
  behaviour: {
    obtainNested: function obtainNestedFields(input) {
      return { fields: { context: 'field', nested: input.fields } };
    }
  },
  contributes: [
    {
      name: 'compileSteps',
      value: {
        on: 'skrOption',
        require: [ 'skrOptions' ],
        compile: compileOption
      }
    }
  ]
};

module.exports = {
  optionTrait: optionTrait,
  obtainNestedOptions: obtainNestedOptions,
  skrOptions: skrOptions
};

function obtainNestedOptions(input) {
  return {
    options: {
      context: 'option',
      nested: input.choices
    }
  };
}

function skrOptions(env, descr) {
  var options = descr.options = [];
  var optionProperties = descr.optionProperties = {};
  var optionDescriptions = descr.optionDescriptions = {};
  env.skrOptions = {
    addOption: function addOption(optionDescription) {
      options.push(optionDescription.name);
      optionDescriptions[optionDescription.name] = optionDescription;
    },
    addProperty: function addProperty(optionName, property, propertyName) {
      var properties = optionProperties[optionName];
      if (!properties) {
        optionProperties[optionName] = properties = {};
      }
      properties[propertyName] = property;
    }
  };
}

function compileOption(env, config, skrOptions) {
  var optionName = config.name;
  var optionDescription = { name: optionName };
  helpers.setLabelAndHelp(optionDescription, config);
  skrOptions.addOption(optionDescription);
  // intercept properties to mark them as dependent on the current
  // choice option
  // we do this in the parent env, we we assume the skrChoice is
  // to compensate for skrPropertyName that also searches in the parent env
  Object.getPrototypeOf(env).skrObject = {
    addProperty: function addProperty(property, propertyName) {
      skrOptions.addProperty(optionName, property, propertyName);
    }
  };
  env.skrOption = { name: optionName };
  if (config.default === true) {
    optionDescription.optionDefault = true;
  }
}
