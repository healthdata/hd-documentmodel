/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');
var helpers = require('../core/helpers');
var skrOption = require('./skrOption');

var choiceTrait = {
  requires: [ 'field' ],
  provides: [ 'obtainNested', 'choiceField' ],
  behaviour: {
    obtainNested: skrOption.obtainNestedOptions
  },
  contributes: [
    {
      name: 'descriptionBuilders',
      value: skrChoice
    },
    {
      name: 'compileSteps',
      value: function (env) {
        skrOption.skrOptions(env, env.skrDescription);
      }
    }
  ]
};

module.exports = choiceTrait;

function skrChoice(skrDescription, config) {
  skrDescription.type = 'choice';
  helpers.setLabelAndHelp(skrDescription, config);
  skrDescription.emptyPredicates.push(function (data) {
    if (skrDescription.isAbsent(data)) { return true; }
    if (_.isObject(data)) {
      return !_.has(data, 'selectedOption') ||
             skrDescription.isAbsent(data.selectedOption);
    }
    return false;
  });
  skrDescription.assertions.choice = {
    check: function checkChoice(data) {
      if (skrDescription.isAbsent(data)) {
        return 'pending';
      }
      if (!_.isObject(data)) {
        return 'failed';
      }
      if (skrDescription.isAbsent(data.selectedOption)) {
        return 'pending';
      }
      if (_.contains(skrDescription.options, data.selectedOption)) {
        return 'ok';
      }
      return 'failed';
    }
  };

  skrDescription.addManipulatorBuilder(
    function choiceManipulatorBuilder(manipulator) {

      manipulator.acceptComputedValue = function (newValue) {
        if (!_.contains(skrDescription.options, newValue)) {
          newValue = undefined;
        }
        manipulator.selectedOption = newValue;
      };

      function defaultOption() {
        var b = skrDescription.behaviour;
        if (!b.defaultValue) { return undefined; }
        return b.defaultValue(manipulator);
      }

      // we need to initialize once because even the automatic intialization
      // by the generic manipulator code requires the presence of a valid value
      manipulator.initialize({ selectedOption: defaultOption() }, true);

      helpers.wrapReinitialization(manipulator, function () {
        if (!manipulator.selectedOption) {
          manipulator.selectedOption = defaultOption();
        }
      });

      manipulator.clearSpecific = function clearChoice() {
        // the following will automatically clear all nested properties
        manipulator.selectedOption = undefined;
      };
      Object.defineProperty(manipulator, 'selectedOption', {
        get: function () {
          var data = manipulator.read();
          if (!_.isObject(data)) { return; }
          return data.selectedOption;
        },
        set: function (v) {
          if (manipulator.selectedOption !== v) {
            manipulator.edit(function (original) {
              if (!_.isObject(original)) {
                // By overwriting the original, we may lose data ...
                original = {};
              }
              original.selectedOption = v;
              return original;
            });
            // clear all manipulators of all the other options
            _.each(manipulator.nestedManipulators,
              function (nestedManipulator) {
                // isActive will trigger the appropriate operations,
                // such as clearing and reinitialization
                nestedManipulator.isActive();
              });
          }
        }
      });
      manipulator.optionManipulators = {};
      _.each(skrDescription.options, function (optionName) {
        var option = manipulator.optionManipulators[optionName] = {};
        Object.defineProperty(option, 'isSelected', {
          get: function () { return manipulator.selectedOption === optionName; }
        });
        function isActive() {
          return option.isSelected;
        }
        option.propertyManipulators = helpers.propertyManipulators(
          skrDescription.optionProperties[optionName],
          manipulator,
          isActive);
      });
      manipulator.nestedManipulators = [];
      _.each(manipulator.optionManipulators, function (om) {
        _.each(om.propertyManipulators, function (m) {
          manipulator.nestedManipulators.push(m);
        });
      });
    }
  );
}
