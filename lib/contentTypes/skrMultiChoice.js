/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');
var helpers = require('../core/helpers');
var skrOption = require('./skrOption');

var multichoiceTrait = {
  requires: [ 'field' ],
  provides: [ 'obtainNested', 'multichoiceField' ],
  behaviour: {
    obtainNested: skrOption.obtainNestedOptions
  },
  contributes: [
    {
      name: 'descriptionBuilders',
      value: skrMultiChoice
    },
    {
      name: 'compileSteps',
      value: function (env) {
        skrOption.skrOptions(env, env.skrDescription);
      }
    }
  ]
};

module.exports = multichoiceTrait;

function skrMultiChoice(skrDescription, config) {
  skrDescription.type = 'multichoice';
  helpers.setLabelAndHelp(skrDescription, config);
  skrDescription.emptyPredicates.push(function (data) {
    if (skrDescription.isAbsent(data)) { return true; }
    if (_.isObject(data)) {
      return !_.has(data, 'selectedOptions') ||
        _.every(skrDescription.options, function (optionName) {
          return !data.selectedOptions[optionName];
        });
    }
    return false;
  });
  skrDescription.assertions.multichoice = {
    check: function checkChoice(data) {
      if (skrDescription.isAbsent(data)) {
        return 'pending';
      }
      if (!_.isObject(data)) {
        return 'failed';
      }
      if (skrDescription.isAbsent(data.selectedOptions)) {
        return 'pending';
      }
      var pred = _.every(data.selectedOptions,
        function (isSelected, option) {
          return _.contains(skrDescription.options, option);
        });
      if (pred) {
        return 'ok';
      }
      return 'failed';
    }
  };

  skrDescription.addManipulatorBuilder(
    function multiChoiceManipulatorBuilder(manipulator) {
      manipulator.acceptComputedValue = function (newValue) {
        if (!_.isObject(newValue)) { return; }
        _.each(manipulator.optionManipulators, function (option, optionName) {
          if (_.has(newValue, optionName)) {
            option.isSelected = Boolean(newValue[optionName]);
          }
        });
      };
      manipulator.initialize({
        selectedOptions: _.object(_.map(skrDescription.options,
          function (optionName) {
            var optionDescription = skrDescription.optionDescriptions[optionName];
            var defaultValue = Boolean(optionDescription.optionDefault);
            return [ optionName, defaultValue ];
          }))
      }, true);
      // Make sure there is always a selectedOptions property
      var choiceObj = manipulator.read();
      if (_.isObject(choiceObj)) {
        choiceObj.selectedOptions = choiceObj.selectedOptions || {};
      }
      manipulator.clearSpecific = function clearMultiChocie() {
        // the following will automatically clear all nested properties
        _.each(manipulator.optionManipulators, function (optionManipulator) {
          optionManipulator.isSelected = false;
        });
      };
      manipulator.optionManipulators = {};
      _.each(skrDescription.options, function (optionName) {
        var option = manipulator.optionManipulators[optionName] = {};
        Object.defineProperty(option, 'isSelected', {
          get: function () { return Boolean(manipulator.read().selectedOptions[optionName]); },
          set: function (v) {
            // skip assignment when the value is already as expected
            if (v === Boolean(manipulator.read().selectedOptions[optionName])) {
              return;
            }
            manipulator.edit(function (choice) {
              choice.selectedOptions[optionName] = Boolean(v);
              return choice;
            });
            if (!v) {
              helpers.clearInactiveManipulators(
                manipulator.optionManipulators[optionName].propertyManipulators);
            }
          }
        });
        function isActive() {
          return option.isSelected;
        }
        option.propertyManipulators = helpers.propertyManipulators(
          skrDescription.optionProperties[optionName],
          manipulator,
          isActive);
      });
      manipulator.nestedManipulators = [];
      _.each(manipulator.optionManipulators, function (om) {
        _.each(om.propertyManipulators, function (m) {
          manipulator.nestedManipulators.push(m);
        });
      });
      helpers.wrapReinitialization(manipulator, function () {
        _.each(skrDescription.options, function (optionName) {
          var optionDescription = skrDescription.optionDescriptions[optionName];
          var defaultValue = Boolean(optionDescription.optionDefault);
          manipulator.optionManipulators[optionName].isSelected = defaultValue;
        });
      });
    }
  );
}
