/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');
var helpers = require('../core/helpers');

var numberTrait = {
  requires: [ 'field' ],
  provides: [ 'numberField' ],
  contributes: [
    {
      name: 'descriptionBuilders',
      value: skrNumber
    }
  ]
};

module.exports = numberTrait;

/**
 * Configure a description for a number field
 * @param  {Description} skrDescription the description to be configured
 * @param  {object}      config         configuration parameters
 */
function skrNumber(skrDescription, config) {
  skrDescription.type = 'number';
  helpers.setLabelAndHelp(skrDescription, config);
  helpers.simpleValue(skrDescription);
  skrDescription.assertions.number = {
    check: function checkNumber(data) {
      if (skrDescription.isAbsent(data)) {
        return 'pending';
      }
      return typeof data === 'number' ? 'ok' : 'failed';
    }
  };

  // for numbers we treat NaN the same as null and undefined
  skrDescription.absentPredicates.push(function (data) {
    // The typeof check is necessary because isNaN('hello') returns true
    // Oh, JavaScript ...
    return typeof data === 'number' && isNaN(data);
  });

  skrDescription.messages.number = 'expected a number';

  var minimum = config.minimum;
  var minimumLevel;
  var maximum = config.maximum;
  var maximumLevel;

  _.each(config.validators, function (validator) {
    if (_.has(validator, 'minimum')) { minimum = validator.minimum; }
    if (_.has(validator, 'maximum')) { maximum = validator.maximum; }
    if (_.has(validator, 'errorMessage')) {
      if (_.has(validator, 'minimum')) {
        skrDescription.messages.minimum = validator.errorMessage;
      }
      if (_.has(validator, 'maximum')) {
        skrDescription.messages.maximum = validator.errorMessage;
      }
    }
    if (validator.level === 'warning') {
      if (_.has(validator, 'minimum')) { minimumLevel = 'warning'; }
      if (_.has(validator, 'maximum')) { maximumLevel = 'warning'; }
    }
  });

  if (typeof minimum !== 'undefined') {
    skrDescription.assertions.minimum = {
      minimum: minimum,
      level: minimumLevel,
      check: function checkMinimum(data) {
        // FIXME this test duplicates the number check
        // we need a way to express that this check depens on the number
        // check
        // The same problem holds for many other validations
        if (skrDescription.isAbsent(data) || typeof data !== 'number') {
          return 'pending';
        }
        return this.minimum <= data ? 'ok' : 'failed';
      }
    };
  }

  if (typeof maximum !== 'undefined') {
    skrDescription.assertions.maximum = {
      maximum: maximum,
      level: maximumLevel,
      check: function checkMaximum(data) {
        if (skrDescription.isAbsent(data) || typeof data !== 'number') {
          return 'pending';
        }
        return data <= this.maximum ? 'ok' : 'failed';
      }
    };
  }
}
