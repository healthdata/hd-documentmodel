/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var helpers = require('../core/helpers');

var booleanTrait = {
  requires: [ 'field' ],
  provides: [ 'booleanField' ],
  contributes: [
    {
      name: 'descriptionBuilders',
      value: skrBoolean
    }
  ]
};

module.exports = booleanTrait;

function skrBoolean(skrDescription, config) {
  skrDescription.type = 'boolean';
  helpers.setLabelAndHelp(skrDescription, config);

  skrDescription.addManipulatorBuilder(
    function (manipulator) {
      Object.defineProperty(manipulator, 'value', {
        get: function () {
          var actualValue = manipulator.read();
          if (actualValue === '0') { return false; }
          if (actualValue === '1') { return true; }
          return actualValue;
        },
        set: function (v) { manipulator.edit(function () { return v; }); }
      });
    });
  helpers.simpleDefaultValue(skrDescription);

  skrDescription.assertions.boolean = {
    check: function checkBoolean(data) {
      if (skrDescription.isAbsent(data)) {
        return 'pending';
      }
      if (data === '0') { return 'ok'; }
      if (data === '1') { return 'ok'; }
      return typeof data === 'boolean' ? 'ok' : 'failed';
    }
  };
}
