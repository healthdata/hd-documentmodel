/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');
var moment = require('moment');
var helpers = require('../core/helpers');

var dateTrait = {
  requires: [ 'field' ],
  provides: [ 'dateField' ],
  contributes: [
    {
      name: 'descriptionBuilders',
      value: skrDate
    }
  ]
};

module.exports = dateTrait;

function skrDate(skrDescription, config) {
  skrDescription.type = 'date';
  helpers.setLabelAndHelp(skrDescription, config);

  /**
   * Parse a given string input into a moment object. This function always returns
   * a moment object, but it should be verified using moment#isValid().
   * Only ISO 8601 formatted input is accepted. Furthermore, this function is independent
   * of the local time zone. When the input contains time zone information, it is respected.
   * When there is no time zone information, UTC is assumed. For example, a date without
   * a time will be interpreted as midnight in UTC for that date.
   */
  skrDescription.asMoment = function (input) {
    if (typeof input !== 'string' || input === '') {
      return moment.invalid();
    }
    var d = moment.utc(input, moment.ISO_8601, true /* strict parsing */).parseZone();
    // Due to a bug in earlier versions of the document model, dates were sometimes
    // rewritten to the day before, in the evening. If the input data has a non-zero
    // hour, after 12, we therefore convert the date to midnight the day after.
    // Note that this rewriting will not have any impact on new documents created with
    // the current version of the document model, since those will not have times included
    // in the date. This code has only an effect on documents created with an older
    // version that did include a time.
    if (d.hour() > 12) {
      d.hour(24);
    }
    return d;
  };

  /**
   * Ignore any time information, only retain the date,
   * making sure that we do not change the date because of a
   * difference in timezone, but do use the local time zone
   * for the result.
   */
  skrDescription.onlyDate = function (m) {
    if (!m.isValid()) {
      return m;
    }
    return moment(m.format('YYYY-MM-DD'));
  };

  skrDescription.assertions.date = {
    check: function checkDate(data) {
      if (skrDescription.isAbsent(data)) {
        return 'pending';
      }
      return skrDescription.asMoment(data).isValid() ? 'ok' : 'failed';
    }
  };
  skrDescription.addManipulatorBuilder(
    function (manipulator) {

      function init() {
        // If the whole document is read-only, we do nothing;
        // if only this manipulator is read-only, we override.
        if (!manipulator.document.readOnly) {
          var hasDefaultValue = skrDescription.behaviour.hasDefaultValue &&
            skrDescription.behaviour.hasDefaultValue(skrDescription);
          if (hasDefaultValue) {
            var defaultValue = skrDescription.behaviour.defaultValue(manipulator);
            var m;
            if (_.isString(defaultValue)) {
              m = moment(defaultValue, [ 'D/M/YYYY', 'YYYY/M/D' ]);
            } else {
              if (_.isDate(defaultValue)) {
                m = moment(defaultValue);
              }
            }
            if (m && m.isValid()) {
              manipulator.initialize(m.format('YYYY-MM-DD'), true);
            }
          }
        }
      }
      helpers.wrapReinitialization(manipulator, init);
      init();

      // We cache the last returned date, in order to obtain object identity
      // for dates.
      var cachedDate = moment.invalid();
      var cachedResult = cachedDate.toDate();
      Object.defineProperty(manipulator, 'value', {
        get: function () {
          var value = manipulator.read();
          var m = skrDescription.onlyDate(manipulator.description.asMoment(value));
          if (!m.isValid()) {
            return value;
          }
          if (!m.isSame(cachedDate)) {
            // the cached date is no good anymore
            cachedDate = m;
            cachedResult = cachedDate.toDate();
          }
          return cachedResult;
        },
        set: function (v) {
          manipulator.edit(function () {
            if (v instanceof Date) {
              var m = moment(v);
              // invalid dates are ignored
              if (!m.isValid()) {
                return undefined;
              }
              cachedDate = skrDescription.onlyDate(moment(v));
              cachedResult = cachedDate.toDate();
              // we use moment.js to format the date as ISO
              return cachedDate.format('YYYY-MM-DD');
            }
            return v;
          });
        }
      });

      manipulator.format = function (format, invalid) {
        invalid = typeof invalid === 'string' ? invalid : '';
        var date = manipulator.value;
        var m = moment(manipulator.value); // This line may give trouble when date equals undefined.
        return date && m.isValid() ? m.format(format) : invalid || '';
      };
    });
}
