/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var helpers = require('../core/helpers');

var textTrait = {
  requires: [ 'field' ],
  provides: [ 'textField' ],
  contributes: [
    {
      name: 'descriptionBuilders',
      value: skrText
    }
  ]
};

module.exports = textTrait;

function skrText(skrDescription, config) {
  skrDescription.type = 'text';
  helpers.setLabelAndHelp(skrDescription, config);
  skrDescription.emptyPredicates.push(function (str) { return str === ''; });
  helpers.simpleValue(skrDescription);
  skrDescription.assertions.text = {
    check: function checkText(data) {
      if (skrDescription.isAbsent(data)) {
        return 'pending';
      }
      return typeof data === 'string' ? 'ok' : 'failed';
    }
  };
  var regexp = config.regexp;
  if (regexp) {
    if (!(regexp instanceof RegExp)) {
      throw new Error('skrRegexp: expected a regular expression, got ' + regexp);
    }
    skrDescription.assertions.regexp = {
      regexp: regexp,
      check: function checkRegexp(data) {
        if (skrDescription.isAbsent(data) || typeof data !== 'string') {
          return 'pending';
        }
        return this.regexp.test(data) ? 'ok' : 'failed';
      }
    };
  }
}
