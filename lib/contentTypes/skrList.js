/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');
var helpers = require('../core/helpers');

var listTrait = {
  requires: [ 'field' ],
  provides: [ 'obtainNested', 'listField' ],
  behaviour: {
    obtainNested: function (input) {
      return {
        listElem: {
          context: 'field',
          nested: [
            _.extend({
              fields: input.fields,
              sections: input.sections
            },
            input.elementConditions ? { conditions: input.elementConditions } : {},
            input.element)
          ]
        }
      };
    }
  },
  contributes: [
    {
      name: 'descriptionBuilders',
      value: skrList
    },
    {
      name: 'compileSteps',
      value: {
        require: [ 'skrDescription' ],
        compile: compileListElem
      }
    }
  ]
};

module.exports = listTrait;

function skrList(skrDescription, config) {
  skrDescription.type = 'list';
  helpers.setLabelAndHelp(skrDescription, config);
  skrDescription.assertions.list = {
    check: function checkList(data) {
      if (skrDescription.isAbsent(data)) {
        return 'pending';
      }
      return _.isArray(data) ? 'ok' : 'failed';
    }
  };
  skrDescription.addManipulatorBuilder(
    function listManipulatorBuilder(manipulator) {
      var initialLength = config.initialLength || 0;
      manipulator.initialize(new Array(initialLength), true);
      manipulator.clearSpecific = function clearList() {
        if (_.isEmpty(manipulator.read())) { return; }
        manipulator.edit(function () {
          return [];
        });
      };
      helpers.wrapReinitialization(manipulator, function () {
        var value = manipulator.read();
        if (_.isUndefined(value) || _.isEmpty(value)) {
          manipulator.edit(function () {
            return new Array(initialLength);
          });
        }
      });
      function elementManipulator(position) {
        var elementDescription = skrDescription.nestedDescriptions.listElement;
        var m = manipulator.propertyManipulator(position, elementDescription);
        m.deleteElement = function deleteElement() {
          manipulator.edit(function (list) {
            list.splice(position, 1);
            recreateElementManipulators(list);
            return list;
          });
        };
        return m;
      }
      var elementManipulators;
      var isRecreating = false;
      function recreateElementManipulators(elements) {
        // do not re-enter
        if (isRecreating) { return; }
        isRecreating = true;
        _.each(elementManipulators, function (em) { em.destroy(); });
        elementManipulators = _.range(elements.length).map(
          function (pos) {
            return elementManipulator(pos);
          });
        manipulator.nestedManipulators = elementManipulators;
        isRecreating = false;
      }
      Object.defineProperty(manipulator, 'elementManipulators', {
        get: function () {
          var elements = manipulator.read();
          var ok = elementManipulators &&
                   elementManipulators.length === elements.length;
          if (!ok) {
            recreateElementManipulators(elements);
          }
          return elementManipulators;
        }
      });

      // trigger the computation of the elementManipulators
      recreateElementManipulators(manipulator.read());

      manipulator.addElement = function addElement(value) {
        manipulator.edit(function (list) {
          list.push(value);
          recreateElementManipulators(list);
          return list;
        });
      };

      manipulator.setLength = function (length) {
        manipulator.edit(function (list) {
          list.length = length;
          recreateElementManipulators(list);
          return list;
        });
      };

      manipulator.countAsField = false;
    }
  );
}

function compileListElem(env, config, description) {
  return function (nestedEnvs) {
    description.nestedDescriptions.listElement = nestedEnvs.listElem[0].skrDescription;
  };
}
