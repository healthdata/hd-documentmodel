/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');
var helpers = require('../core/helpers');

var objectTrait = {
  requires: [ 'field' ],
  provides: [ 'obtainNested', 'objectField' ],
  behaviour: {
    obtainNested: obtainNestedFieldsAndSections
  },
  contributes: [
    {
      name: 'descriptionBuilders',
      value: skrObject
    },
    {
      name: 'compileSteps',
      value: compileObject
    }
  ]
};

var propertyNameTrait = {
  requires: [ 'field' ],
  contributes: [
    {
      name: 'compileSteps',
      value: {
        on: 'skrPropertyName',
        require: [ 'skrDescription' ],
        compile: compilePropertyName
      }
    }
  ]
};

module.exports = {
  skrObject: skrObject,
  compileObject: compileObject,
  objectTrait: objectTrait,
  propertyNameTrait: propertyNameTrait
};

function obtainNestedFieldsAndSections(input) {
  return {
    fields: {
      context: 'field',
      nested: input.fields
    },
    sections: {
      context: 'section',
      nested: input.sections
    }
  };
}

function compileObject(env) {
  var skrDescription = env.skrDescription;
  env.skrObject = {
    addProperty: function addProperty(property, propertyName) {
      // sanity check: avoids infinite loops
      if (property === skrDescription) {
        throw new Error(
          'Circular description when adding ' + propertyName + ' to ' + skrDescription.label +
          '. Perhaps a missing data directive?');
      }
      skrDescription.nestedDescriptions[propertyName] = property;
    }
  };
}

function compilePropertyName(env, config, skrDescription) {
  var propertyName = config.name;
  if (!propertyName) { return; }
  // FIXME the 'require' property on directives does not support searching
  // in the *parent* environment
  var parentEnv = Object.getPrototypeOf(env);
  var skrObject = parentEnv.skrObject;
  if (!skrObject) {
    // There is no object to add this field to.
    return;
  }
  skrObject.addProperty(skrDescription, config.name);
}

function skrObject(skrDescription, config) {
  skrDescription.type = 'object';
  helpers.setLabelAndHelp(skrDescription, config);
  skrDescription.assertions.object = {
    check: function checkObject(data, manipulator) {
      if (skrDescription.isAbsent(data, manipulator)) {
        return 'pending';
      }
      return _.isObject(data) ? 'ok' : 'failed';
    }
  };
  skrDescription.addManipulatorBuilder(
    function objectManipulatorBuilder(manipulator) {
      manipulator.initialize(_.object(_.map(
        skrDescription.nestedDescriptions,
        function (nested, propertyName) {
          return [ propertyName, undefined ];
        })), true);
      manipulator.clearSpecific = function clearObject() {
        helpers.clearManipulators(manipulator.propertyManipulators);
      };
      manipulator.countAsField = false;
      manipulator.propertyManipulators = helpers.propertyManipulators(
        skrDescription.nestedDescriptions, manipulator);
      manipulator.nestedManipulators = _.values(manipulator.propertyManipulators);
    }
  );
}
