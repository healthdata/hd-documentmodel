/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');

function autocompleteTrait(options) {
  var builder = {
    on: options.on,
    require: options.require,
    build: function (skrDescription, config) {
      // get the results of options.require by removing
      // skrDescription and condig from the arguments
      var required = _.rest(arguments, 2);

      skrDescription.autoCompleters.push({
        name: options.on,
        autocomplete: acFn
      });
      function acFn(manipulator, inputData) {
        // invoke the autocomplete function with the correct arguments
        // including the required directives
        var args = [ manipulator, inputData, config ].concat(required);
        return (options.autocomplete).apply(null, args);
      }
    }
  };
  return {
    contributes: [
      {
        name: 'descriptionBuilders',
        value: builder
      }
    ]
  };
}

function importBySchema(document, schemaName, inputData) {
  var description = document.root.description;
  var schemaConfig = description.schemas[schemaName];
  if (!schemaConfig) {
    throw new Error('importBySchema: no schema with given name ' + schemaName);
  }
  document.traverseWithContext(inputData, function (manipulator, context) {
    var description = manipulator.description;
    var mapping = description.mappings[schemaName];
    // If this field has a mapping, we invoke it and use the resulting context(s)
    // for any nested fields; if not, we reuse the current context for the
    // nested fields.
    return mapping ?
      manipulator.importBySchema(schemaConfig, mapping, context) :
      null; // By returning null, traverseWithContext will reuse the context
  });
}

module.exports.autocompleteTrait = autocompleteTrait;
module.exports.importBySchema = importBySchema;
