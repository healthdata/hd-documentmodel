/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var moment = require('moment');
var _ = require('underscore');

var traits = require('./traits');
var csvHelpers = require('./csvHelpers');
var autocompleteTrait = require('./autocomplete').autocompleteTrait;

var csvTraits = {};

module.exports = _.extend({ traits: csvTraits }, csvHelpers);

var csvData = csvHelpers.csvData;
var pathAsString = csvHelpers.pathAsString;
var simpleValueBuildCSV = csvHelpers.simpleValueBuildCSV;
var simpleValueAutocomplete = csvHelpers.simpleValueAutocomplete;
var join = csvHelpers.join;
var split = csvHelpers.split;

function simpleValueCSV(config) {
  csvTraits[config.on] = csvHelpers.simpleValueCSV(config);
}

simpleValueCSV({
  on: 'skrBoolean',
  importFromCSV: function (manipulator, value) {
    if (value === 'TRUE') {
      manipulator.value = true;
      return true;
    }
    if (value === 'FALSE') {
      manipulator.value = false;
      return true;
    }
  },
  exportToCSV: function (manipulator) {
    var value;
    if (manipulator.assertions.boolean === 'ok') {
      value = manipulator.value ? 'TRUE' : 'FALSE';
    }
    return value;
  }
});

// a decimal with comma
var NUMBER_REGEXP = /^\s*(\-|\+)?(\d+|(\d*(\,\d*)))\s*$/;

simpleValueCSV({
  on: 'skrNumber',
  importFromCSV: function (manipulator, value) {
    // convert the number from comma-decimal to point-decimal
    if (NUMBER_REGEXP.test(value)) {
      value = value.replace(',', '.');
      manipulator.value = parseFloat(value);
      return true;
    }
  },
  exportToCSV: function (manipulator) {
    if (manipulator.assertions.number === 'ok') {
      return String(manipulator.value).replace('.', ',');
    }
    return undefined;
  }
});

simpleValueCSV({
  on: 'skrText',
  importFromCSV: function (manipulator, value) {
    if (value === '') { return; }
    manipulator.value = value;
    return true;
  },
  exportToCSV: function (manipulator) {
    return manipulator.value;
  }
});

simpleValueCSV({
  on: 'skrEmail',
  importFromCSV: function (manipulator, value) {
    if (value === '') { return; }
    manipulator.value = value;
    return true;
  },
  exportToCSV: function (manipulator) {
    return manipulator.value;
  }
});

var skrChoiceCompileStep = {
  on: 'skrChoice',
  require: [ 'skrDescription' ],
  compile: function (env, config, skrDescription) {
    simpleValueBuildCSV(skrDescription);
    skrDescription.addManipulatorBuilder(function (manipulator) {
      manipulator.importFromCSV = function (value) {
        if (_.contains(manipulator.description.options, value)) {
          manipulator.selectedOption = value;
          return true;
        }
      };
      manipulator.exportToCSV = function () {
        var value;
        if (manipulator.assertions.choice === 'ok') {
          value = manipulator.selectedOption;
        }
        return value;
      };
    });
    return function () {
      // When the choice has no nested fields, treat as a simple value.
      var nested = skrDescription.nestedDescriptions;
      if (Object.keys(nested).length === 0) {
        skrDescription.isSimpleValue = true;
      }
    };
  }
};

(function () {
  // setup csv support for skrChoice
  var choiceTrait = {
    contributes: [
      {
        name: 'compileSteps',
        value: skrChoiceCompileStep
      }
    ]
  };
  var acTrait = autocompleteTrait({
    on: 'skrChoice',
    autocomplete: simpleValueAutocomplete
  });

  csvTraits.skrChoice = traits.compose(choiceTrait, acTrait);
})();

var multichoiceAutocompleteConfig = {
  on: 'skrMultiChoice',
  autocomplete: function (manipulator, inputData, config) {
    /* jshint unused: false */
    if (!inputData.csv) { return; }
    var separator = manipulator.description.separator;
    var result = csvData(inputData, manipulator);
    if (typeof result !== 'string') { return; }
    if (result === '') { return; }
    result = split(result, separator);
    _.each(manipulator.optionManipulators, function (optionManipulator, optionName) {
      optionManipulator.isSelected = _.contains(result, optionName);
    });
    return true;
  }
};

var skrMultiChoiceCompileStep = {
  on: 'skrMultiChoice',
  require: [ 'skrDescription' ],
  compile: function (env, config, skrDescription) {
    skrDescription.separator = config.separator || '|';
    skrDescription.buildCSV = function (manipulator, csvBuilder) {
      var key = pathAsString(manipulator.path);
      if (manipulator.assertions.multichoice === 'ok') {
        var selectedOptions = [];
        _.each(manipulator.optionManipulators,
          function (optionManipulator, optionName) {
            if (optionManipulator.isSelected) {
              selectedOptions.push(optionName);
            }
          });
        csvBuilder.addColumn(key, join(selectedOptions, skrDescription.separator));
      }
    };
  }
};

(function () {
  // setup csv support for skrMultiChoice
  var multichoiceTrait = {
    contributes: [
      {
        name: 'compileSteps',
        value: skrMultiChoiceCompileStep
      }
    ]
  };
  var acTrait = autocompleteTrait(multichoiceAutocompleteConfig);

  csvTraits.skrMultiChoice = traits.compose(multichoiceTrait, acTrait);
})();

simpleValueCSV({
  on: 'skrDate',
  exportToCSV: function (manipulator) {
    var value;
    if (manipulator.assertions.date === 'ok') {
      var date = moment(manipulator.value);
      value = date.format('DD/MM/YYYY');
    }
    return value;
  },
  importFromCSV: function (manipulator, value) {
    var d = moment(value, 'DD/MM/YYYY');
    if (!d.isValid()) { return; }
    manipulator.value = d.toDate();
    return true;
  }
});

var listAutocompleteConfig = {
  on: 'skrList',
  autocomplete: function (manipulator, inputData, config) {
    /* jshint unused: false */
    if (!inputData.csv) { return; }
    var separator = manipulator.description.separator;

    if (manipulator.description.isSimpleValueList) {
      var result = csvData(inputData, manipulator);
      if (typeof result !== 'string') { return; }
      if (result === '') { return; }
      var values = split(result, separator);
      manipulator.edit(function (arr) {
        // give the array the correct size
        arr.length = values.length;
        return arr;
      });

      _.each(manipulator.elementManipulators, function (em, position) {
        var fieldManipulator = em.nestedManipulators[0];
        fieldManipulator.csvImportedByParent =
          fieldManipulator.importFromCSV(values[position]);
      });

      // skip the rest of the function, which implements the general
      // list autocompletion
      return true;
    }

    // to figure out the length of the list, we need to inspect all the column names
    var columnNames = Object.keys(inputData.csv);
    var key = pathAsString(manipulator.path);
    // filter those column names that start with the list's key
    // AND that have a value in the input data
    var listColumnNames = _.filter(columnNames, function (columnName) {
      return columnName.lastIndexOf(key, 0) === 0 &&
             inputData.csv[columnName] !== '';
    });
    // determine how large the list should be based on the referred indices
    var requiredSizes = _.map(listColumnNames, function (listColumnName) {
      // drop the key prefix
      listColumnName = listColumnName.substr(key.length);
      var match = listColumnName.match(/^\|(\d+)\|/);
      if (match) {
        // if element n is referred, the list should have a length of
        // at least n+1
        return Number(match[1]) + 1;
      }
      return 0;
    });
    var size = Math.max(0, _.max(requiredSizes));

    // give the array the correct size
    // initialization of the elements happens automatically when referring to
    // manipulator.elementManipulators
    manipulator.edit(function (arr) {
      arr.length = size;
      // needed to recreate the element manipulators immediately
      _.identity(manipulator.elementManipulators);
      return arr;
    });

    return true;
  }
};

var skrListCompileStep = {
  on: 'skrList',
  require: [ 'skrDescription' ],
  compile: function (env, node, skrDescription) {
    var separator = node.separator || '|';
    skrDescription.separator = separator;
    env.simpleValueList = function () {
      skrDescription.isSimpleValueList = true;
      skrDescription.buildCSV = function (manipulator, csvBuilder) {
        var key = pathAsString(manipulator.path);
        var values = [];
        _.each(manipulator.elementManipulators, function (em) {
          // it is expect that all elements of the list are objects
          // with a single property
          em = em.nestedManipulators[0];
          values.push(em.exportToCSV());
        });
        csvBuilder.addColumn(key, join(values, separator));
      };
    };
  }
};

var skrListElementCompileStep = {
  on: 'skrListElement',
  require: [ 'simpleValueList' ],
  compile: function (env, node, simpleValueList) {
    return function (nestedEnvs) {
      var skrDescription = nestedEnvs.listElem[0].skrDescription;
      // Normally, lists always have objects as elements.
      // If not, we abort, and the list is treated normally
      if (skrDescription.type !== 'object') { return; }
      var properties = Object.keys(skrDescription.nestedDescriptions);
      if (properties.length === 1) {
        var onlyProperty = skrDescription.nestedDescriptions[properties[0]];
        if (onlyProperty.isSimpleValue) {
          onlyProperty.skipCSVExport = true;
          simpleValueList();
        }
      }
    };
  }
};

(function () {
  // setup csv support for skrList
  var listTrait = {
    contributes: [
      {
        name: 'compileSteps',
        value: skrListCompileStep
      },
      {
        name: 'compileSteps',
        value: skrListElementCompileStep
      }
    ]
  };
  var acTrait = autocompleteTrait(listAutocompleteConfig);

  csvTraits.skrList = traits.compose(listTrait, acTrait);
})();
