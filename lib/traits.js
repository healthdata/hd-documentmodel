/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');

module.exports = {
  empty: emptyTrait,
  compose: compose,
  augment: augment,
  wrap: wrap,
  provides: provides
};

function provides(trait, name) {
  return _.contains(trait.provides, name);
}

function methodsOf(trait, forName) {
  return _.compact(_.map(trait.methodsOf, function (name, method) {
    return forName === name ? method : null;
  }));
}

function propertiesOf(trait, typeName) {
  return _.compact(_.map(trait.propertiesOf, function (info, member) {
    return typeName === info.type ?
      { member: member, name: info.name } :
      null;
  }));
}

function disjoint(left, right) {
  return _.intersection(left, right).length === 0;
}

function append(left, right) {
  return (left || []).concat(right || []);
}

function emptyTrait() {
  return {
    requires: [],
    provides: [],
    collects: [],
    behaviour: {},
    propertiesOf: {},
    methodsOf: {},
    contributes: []
  };
}

function allowSingleContribution(value) {
  if (_.isArray(value)) { return value; }
  if (_.isObject(value)) { return [ value ]; }
  return value;
}

function compose(left, right) {
  if (!disjoint(left.provides, right.provides)) {
    throw new Error('traits.compose: traits should have disjoint "provides" properties');
  }
  if (!disjoint(left.collects, right.collects)) {
    throw new Error('traits.compose: traits should have disjoint "collects" properties');
  }
  if (!disjoint(_.keys(left.behaviour), _.keys(right.behaviour))) {
    throw new Error('traits.compose: traits should have disjoint "behaviour" properties');
  }
  var provides = append(left.provides, right.provides);
  return {
    requires: _.difference(_.union(left.requires, right.requires), provides),
    provides: provides,
    collects: append(left.collects, right.collects),
    behaviour: _.extend({}, left.behaviour, right.behaviour),
    propertiesOf: _.extend({}, left.propertiesOf, right.propertiesOf),
    methodsOf: _.extend({}, left.methodsOf, right.methodsOf),
    contributes: append(
      allowSingleContribution(left.contributes),
      allowSingleContribution(right.contributes))
  };
}

function augment(main, extra) {
  var compatible = _.isEmpty(_.difference(extra.requires, main.provides));
  return compatible ? compose(main, extra) : main;
}

function groupedContributions(trait) {
  return _.mapObject(
    _.groupBy(allowSingleContribution(trait.contributes), 'name'),
    function (contributions) { return _.pluck(contributions, 'value'); });
}

function wrap(mainTraits, extraTraits, name) {

  mainTraits = _.flatten(mainTraits);

  // First we combine all main traits into a single trait called 'main'.
  var main = _.reduce(mainTraits, compose, emptyTrait());
  // Then we augment the 'main' trait with all compatible extra traits.
  var composition = _.reduce(extraTraits, augment, main);

  if (!_.isEmpty(composition.requires)) {
    throw new Error('traits.wrap: not all requirements are provided for type ' + name);
  }

  // we 'close' the circular dependencies between the individual traits and the
  // composed result.
  var tieTheKnot = {};
  var publicMethods = _.extend(
    // We use the method name '$provides' to avoid name clashes.
    {
      $provides: _.partial(_.contains, composition.provides),
      $methodsOf: _.partial(methodsOf, composition),
      $propertiesOf: _.partial(propertiesOf, composition)
    },
    _.mapObject(composition.behaviour, function (method) {
      return _.bind(method, tieTheKnot);
    }));
  _.extend(tieTheKnot,
    groupedContributions(composition),
    publicMethods);

  return publicMethods;
}
