Run `npm install` to install dependencies.

Run `npm test` to run the tests.

Run `npm run test:live` for test-driven development.

Run `npm run test:bamboo` to run the tests as they are run by the build server.

Run `npm run coverage` to generate code coverage.

Run `npm run codestyle` to verify code style using JSHint and JSCS.

Run `npm run docco` to generate documentation using Docco.

Run `npm run jsdoc` to generate documentation using JSDoc.

Run `npm run clean` to clean up generated directories.
