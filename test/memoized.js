/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var memoized = require('../lib/memoized');

describe('memoized', function () {

  it('should retain its computed value', function () {
    var spy = sinon.spy(function () { return 1; });
    var m = memoized(spy);
    expect(m.value()).to.equal(1);
    expect(spy).to.have.been.calledOnce;

    // calling m.value() again should not recompute
    expect(m.value()).to.equal(1);
    expect(spy).to.have.been.calledOnce;
  });

  it('should not recompute on null', function () {
    var spy = sinon.spy(function () { return null; });
    var m = memoized(spy);
    expect(m.value()).to.equal(null);
    expect(spy).to.have.been.calledOnce;

    // calling m.value() again should not recompute
    expect(m.value()).to.equal(null);
    expect(spy).to.have.been.calledOnce;
  });

  it('should not recompute on undefined', function () {
    var spy = sinon.spy(function () { return undefined; });
    var m = memoized(spy);
    expect(m.value()).to.equal(undefined);
    expect(spy).to.have.been.calledOnce;

    // calling m.value() again should not recompute
    expect(m.value()).to.equal(undefined);
    expect(spy).to.have.been.calledOnce;
  });

  it('should recompute only after invalidation', function () {
    var i = 1;
    var m = memoized(function () { return i; });
    expect(m.value()).to.equal(1);
    i = 2;
    expect(m.value()).to.equal(1);
    m.invalidate();
    expect(m.value()).to.equal(2);
  });

  it('should detect circularity', function () {
    var m = memoized(function () { return m.value() + 1; });
    expect(function () { m.value(); }).to.throw('memoized: circular dependency');
  });

  it('should create property descriptors', function () {
    var i = 1;
    var m = memoized(function () { return i; });
    var obj = Object.create(Object.prototype, {
      someProperty: m.propertyDescriptor()
    });
    expect(obj.someProperty).to.equal(1);
    i = 2;
    expect(obj.someProperty).to.equal(1);
    m.invalidate();
    expect(obj.someProperty).to.equal(2);

    // test that the property is enumerable
    expect(JSON.stringify(obj)).to.equal('{"someProperty":2}');
  });

});

describe('memoized group', function () {
  it('should invalidate all its elements', function () {
    var group = memoized.group();
    var i = 42;
    var m1 = group.memoized(function () { return i; });
    var m2 = group.memoized(function () { return i + 1; });
    expect(m1.value()).to.equal(42);
    expect(m2.value()).to.equal(43);
    i = 101;
    expect(m1.value()).to.equal(42);
    expect(m2.value()).to.equal(43);
    group.invalidate();
    expect(m1.value()).to.equal(101);
    expect(m2.value()).to.equal(102);
  });
});
