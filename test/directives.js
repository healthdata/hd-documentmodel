/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');
var moment = require('moment');

var parseDocumentDefinition = require('../lib/core/parseDocumentDefinition');
var makeProfile = require('../lib/core/makeProfile');
var makeDocument = require('../lib/core/makeDocument');
var makeDocumentDataSource = require('../lib/core/makeDocumentDataSource');
var makeDescription = require('../lib/core/makeDescription');
var helpers = require('../lib/core/helpers');
var components = require('../lib/components');

var computedExpressionsTrait = require('../lib/modalities/skrComputedExpression');

describe('directives', function () {

  var env, description, document, manipulator, testProfile;

  beforeEach(function () {
    env = {};
    var compileTrait = parseDocumentDefinition.compileTrait;
    var makeDescriptionTrait = parseDocumentDefinition.makeDescriptionTrait;
    var anythingTrait = {
      requires: [ 'field' ],
      provides: [ 'anythingField' ]
    };

    var types = {
      text: 'text',
      number: 'number',
      date: 'date',
      email: 'email',
      boolean: 'boolean',
      anything: 'anything',
      choice: 'choice',
      multichoice: 'multichoice',
      list: 'list'
    };
    testProfile = makeProfile({
      defaultContext: 'field',
      contexts: {
        field: {
          types: types,
          defaultType: 'object'
        },
        option: {
          defaultType: 'option'
        }
      },
      types: {
        anything: [ compileTrait, makeDescriptionTrait, anythingTrait,
          components.editorInputTrait('anything') ],
        text: [ compileTrait, makeDescriptionTrait,
          require('../lib/contentTypes/skrText'),
          components.traits.skrTextInput ],
        email: [ compileTrait, makeDescriptionTrait,
          require('../lib/contentTypes/skrEmail'),
          components.traits.skrTextInput ],
        number: [ compileTrait, makeDescriptionTrait,
          require('../lib/contentTypes/skrNumber'),
          components.traits.skrNumberInput ],
        boolean: [ compileTrait, makeDescriptionTrait,
          require('../lib/contentTypes/skrBoolean'),
          components.traits.skrBooleanInput ],
        date: [ compileTrait, makeDescriptionTrait,
          require('../lib/contentTypes/skrDate'),
          components.traits.skrDateInput ],
        choice: [ compileTrait, makeDescriptionTrait,
          require('../lib/contentTypes/skrChoice'),
          components.traits.skrChoiceInput ],
        multichoice: [ compileTrait, makeDescriptionTrait,
          require('../lib/contentTypes/skrMultiChoice'),
          components.traits.skrMultiChoiceInput ],
        option: [ compileTrait, require('../lib/contentTypes/skrOption').optionTrait,
          components.groups.skrOptionInput,
          components.traits.skrOptionInput ],
        object: [ compileTrait, makeDescriptionTrait,
          require('../lib/contentTypes/skrObject').objectTrait,
          computedExpressionsTrait,
          components.groups.skrObject,
          components.traits.skrObject ],
        list: [ compileTrait, makeDescriptionTrait,
          require('../lib/contentTypes/skrList'),
          computedExpressionsTrait,
          components.traits.skrListInput ]
      },
      modalities: [
        require('../lib/modalities/skrLabel'),
        require('../lib/modalities/skrHelp'),
        require('../lib/modalities/skrDefault'),
        require('../lib/modalities/skrRequired'),
        require('../lib/modalities/skrOnlyWhen'),
        require('../lib/modalities/skrComputed'),
        require('../lib/modalities/skrReadOnly'),
        require('../lib/modalities/skrCondition'),
        require('../lib/modalities/skrSticky'),
        require('../lib/contentTypes/skrObject').propertyNameTrait,
        components.traits.propertyNameTrait
      ]
    });
  });

  function parseDefinition(input) {
    parseDocumentDefinition(input, testProfile).compile(env);
    description = env.skrDescription;
  }

  function manipulate(target, options) {
    var dataSource = makeDocumentDataSource(target);
    document = makeDocument(dataSource, description, options);
    manipulator = document.root;
    return manipulator;
  }

  function manipulateReadOnly(target) {
    var options = { readOnly: true };
    return manipulate(target, options);
  }

  function testAssertions(data, context) {
    var manipulator = {
      read: function () { return data; },
      document: {
        options: {},
        computedExpressionArguments: _.extend(
          helpers.defaultComputedExpressionArguments(),
          { ctx: function () { return context; } })
      },
      options: {}
    };
    return description.testAssertions(data, manipulator);
  }

  function validate(data) {
    return { assertions: testAssertions(data) };
  }

  describe('skrNumber', function () {

    it('should validate numbers', function () {
      parseDefinition({ type: 'number' });
      testAssertions(5).should.eql({ number: 'ok' });
      testAssertions('hello').should.eql({ number: 'failed' });
      testAssertions(NaN).should.eql({ number: 'pending' });
      testAssertions(undefined).should.eql({ number: 'pending' });
      testAssertions(null).should.eql({ number: 'pending' });
    });

    it('should check the minimum value', function () {
      parseDefinition({ type: 'number', minimum: 42 });
      testAssertions(123).should.eql({ number: 'ok', minimum: 'ok' });
      testAssertions(42).should.eql({ number: 'ok', minimum: 'ok' });
      testAssertions(5).should.eql({ number: 'ok', minimum: 'failed' });
      testAssertions('hello')
        .should.eql({ number: 'failed', minimum: 'pending' });
      testAssertions(undefined)
        .should.eql({ number: 'pending', minimum: 'pending' });
      testAssertions(null)
        .should.eql({ number: 'pending', minimum: 'pending' });
    });

    it('should check the maximum value', function () {
      parseDefinition({ type: 'number', maximum: 42 });
      testAssertions(123).should.eql({ number: 'ok', maximum: 'failed' });
      testAssertions(42).should.eql({ number: 'ok', maximum: 'ok' });
      testAssertions(5).should.eql({ number: 'ok', maximum: 'ok' });
      testAssertions('hello')
        .should.eql({ number: 'failed', maximum: 'pending' });
      testAssertions(undefined)
        .should.eql({ number: 'pending', maximum: 'pending' });
      testAssertions(null)
        .should.eql({ number: 'pending', maximum: 'pending' });
    });

    it('should expose minimum and maximum', function () {
      parseDefinition({ type: 'number', minimum: 2, maximum: 42 });
      description.assertions.minimum.minimum.should.equal(2);
      description.assertions.maximum.maximum.should.equal(42);
    });

    it('should accept minimum and maximum of zero', function () {
      parseDefinition({ type: 'number', minimum: 0 });
      description.assertions.should.have.property('minimum');
      parseDefinition({ type: 'number', maximum: 0 });
      description.assertions.should.have.property('maximum');
    });

    it('should expose label and help text', function () {
      parseDefinition({
        type: 'number',
        label: 'Foo',
        help: 'Bar'
      });
      expect(description).to.have.property('label', 'Foo');
      expect(description).to.have.property('help', 'Bar');
    });

    it('should manipulate numbers', function () {
      parseDefinition({ type: 'number' });
      var target = { value: 42 };
      var m = manipulate(target);
      m.value.should.equal(42);
      m.value = 123;
      m.value.should.equal(123);
      target.value.should.equal(123);
    });

    it('should validate continuously via manipulator', function () {
      parseDefinition({ type: 'number', maximum: 100 });
      var target = { value: 42 };
      var m = manipulate(target);
      expect(m.assertions).to.deep.equal({ number: 'ok', maximum: 'ok' });
      expect(m.errors).to.deep.equal([]);
      m.value = 123;
      expect(m.assertions).to.deep.equal({ number: 'ok', maximum: 'failed' });
      expect(m.errors).to.deep.equal([ 'maximum' ]);
      m.assertions.should.equal(m.assertions);
      m.errors.should.equal(m.errors);
    });

    it('should accept a default value', function () {
      parseDefinition({ type: 'number', default: 42 });
      var b = description.behaviour;
      expect(b.hasDefaultValue(description)).to.be.true;
      var target = { value: undefined };
      var m = manipulate(target);
      expect(b.defaultValue(manipulator)).to.equal(42);
      m.value.should.equal(42);
      target.value.should.equal(42);
    });

    it('should accept a computed default value', function () {
      parseDefinition({
        computedExpressions: {
          someComputedNumber: '1 + 2'
        },
        fields: [
          {
            type: 'number',
            name: 'someNumber',
            computedDefault: 'someComputedNumber'
          }
        ]
      });
      var m = manipulate({});
      m.propertyManipulators.someNumber.value.should.equal(3);
    });

    it('should set default messages', function () {
      parseDefinition({ type: 'number' });
      expect(description.messageFor('number'))
        .to.equal('expected a number');
    });
  });

  describe('skrRequired', function () {

    it('should test for the presence of data', function () {
      parseDefinition({ type: 'anything', required: true });
      testAssertions(42).should.eql({ required: 'ok' });
      testAssertions('hello').should.eql({ required: 'ok' });
      testAssertions({}).should.eql({ required: 'ok' });
      testAssertions([]).should.eql({ required: 'ok' });
      testAssertions(undefined).should.eql({ required: 'failed' });
      testAssertions(null).should.eql({ required: 'failed' });
    });

    it('should take the config parameter into account', function () {
      parseDefinition({ type: 'anything', required: false });
      testAssertions(42).should.eql({});
      testAssertions(undefined).should.eql({});
      testAssertions(null).should.eql({});
    });

    it('should fail on the empty string', function () {
      parseDefinition({ type: 'text', required: true });
      testAssertions('').should.eql({ text: 'ok', required: 'failed' });
    });

    it('should provide a default message', function () {
      parseDefinition({ type: 'text', required: true });
      testAssertions('').should.eql({ text: 'ok', required: 'failed' });
    });

    it('should expose on manipulator whether required or not', function () {
      parseDefinition({ type: 'text', required: true });
      manipulate({});
      expect(manipulator).to.have.property('isRequired', true);
    });

    it('should support conditional requiredness', function () {
      parseDefinition({
        fields: [
          { type: 'number', name: 'someNumber' },
          { type: 'text', name: 'someText', required: 'lessThenTen' }
        ],
        computedExpressions: {
          lessThenTen: '$.someNumber < 10'
        }
      });
      manipulate({ value: { someNumber: 11 }});
      var someNumberManipulator = manipulator.propertyManipulators.someNumber;
      var someTextManipulator = manipulator.propertyManipulators.someText;
      expect(manipulator.computedExpressions).to.have.property('lessThenTen', false);
      expect(someTextManipulator.errors).to.deep.equal([]);
      expect(someTextManipulator).to.have.property('isRequired', false);
      someNumberManipulator.value = 3;
      expect(someTextManipulator.errors).to.deep.equal([ 'required' ]);
      expect(someTextManipulator).to.have.property('isRequired', true);
    });
  });

  describe('skrMessage', function () {

    it('should set messages on the current description', function () {
      parseDefinition({
        type: 'number',
        validators: [ { minimum: 10, errorMessage: 'Expected a number between 10 and 100' } ]
      });
      expect(description.messageFor('minimum'))
        .to.equal('Expected a number between 10 and 100');
      expect(description.messageFor('fubar')).to.be.undefined;
    });
  });

  describe('skrDate', function () {

    it('should parse dates without changing them because of timezone differences', function () {
      parseDefinition({ type: 'date' });
      _.each([
        {
          input: '1914-11-10T00:00:00+05:00',
          date: '10/11/1914',
          full: '1914-11-10T00:00:00+05:00'
        },
        {
          input: '1945-11-10T03:00:00-03:00',
          date: '10/11/1945',
          full: '1945-11-10T03:00:00-03:00'
        },
        {
          input: '1987-11-10',
          date: '10/11/1987',
          full: '1987-11-10T00:00:00+00:00'
        }
      ], function (info) {
        var m = description.asMoment(info.input);
        expect(m.format('DD/MM/YYYY')).to.equal(info.date);
        expect(m.format('YYYY-MM-DDTHH:mm:ssZ')).to.equal(info.full);
      });
    });

    it('should convert dates to local time by retaining only the date', function () {
      parseDefinition({ type: 'date' });
      _.each([
        { input: '1914-11-10T11:00:00+05:00', day: 10 },
        { input: '1945-11-10T03:00:00-03:00', day: 10 }
      ], function (info) {
        var m = moment.parseZone(info.input);
        // JavaScript dates are always in local time. Since the main reason for the
        // onlyDate() function is to obtain reliable JavaScript dates, we test the results
        // that way.
        var jsDate = description.onlyDate(m).toDate();
        expect(jsDate.getDate()).to.equal(info.day);
        expect(jsDate.getHours()).to.equal(0);
        expect(jsDate.getMinutes()).to.equal(0);
      });
    });

    it('should correctly interpret dates with non-zero hours', function () {
      // In particular, we want to interpet dates with a time in the evening as the
      // next day, to correct for an earlier bug in the date implementation.
      parseDefinition({ type: 'date' });
      _.each([
        {
          input: '1914-11-10T01:00:00+05:00',
          date: '10/11/1914',
          full: '1914-11-10T01:00:00+05:00'
        },
        {
          input: '1945-11-10T03:00:00-03:00',
          date: '10/11/1945',
          full: '1945-11-10T03:00:00-03:00'
        },
        {
          input: '1914-11-10T22:00:00+05:00',
          date: '11/11/1914',               // the next day!
          full: '1914-11-11T00:00:00+05:00' // midnight the next day!
        },
        {
          input: '1945-11-10T21:00:00-03:00',
          date: '11/11/1945',               // the next day!
          full: '1945-11-11T00:00:00-03:00' // midnight the next day!
        }
      ], function (info) {
        var m = env.skrDescription.asMoment(info.input);
        expect(m.format('DD/MM/YYYY')).to.equal(info.date);
        expect(m.format('YYYY-MM-DDTHH:mm:ssZ')).to.equal(info.full);
      });
    });

    it('should validate dates', function () {
      parseDefinition({ type: 'date' });
      // we expect date strings
      // Date objects are only used as manipulator value
      testAssertions(new Date()).should.eql({ date: 'failed' });
      testAssertions('Mon Sep 22 2014 09:45:43 GMT+0200 (CEST)').should.eql({ date: 'failed' });
      testAssertions('2014-09-22').should.eql({ date: 'ok' });
      testAssertions('2014-09-22T09:45:43+02:00').should.eql({ date: 'ok' });
      testAssertions('hello').should.eql({ date: 'failed' });
      testAssertions(undefined).should.eql({ date: 'pending' });
      testAssertions(null).should.eql({ date: 'pending' });
    });

    it('should manipulate dates', function () {
      parseDefinition({ type: 'date' });
      var target = { value: '2014-09-22T09:45:43+02:00' };
      var m = manipulate(target);
      m.value.should.be.instanceOf(Date);
      // Here, we rely on JavaScript's interpretation of YYYY/MM/DD to return
      // midnight in the local time zone. This is the intended semantics of
      // the "value" property.
      m.value.should.deep.equal(new Date('2014/09/22'));

      var someDate = new Date('Mon Sep 21 2015 10:45:43 GMT+0200 (CEST)');
      // Idem as above.
      var actualDate = new Date('2015/09/21');
      m.value = someDate;

      // these tests are important to reach stability in angular.js
      m.value.should.equal(m.value);
      m.value.should.deep.equal(actualDate);

      target.value.should.be.a('string');
      target.value.should.equal('2015-09-21');

      m.value = undefined;
      expect(m.value).to.be.undefined;
      expect(target.value).to.be.undefined;

      m.value = null;
      expect(m.value).to.be.null;
      expect(target.value).to.be.null;

      m.value = 'this is not a date';
      expect(m.value).to.equal('this is not a date');
      expect(target.value).to.equal('this is not a date');

      m.value = new Date('invalid date');
      expect(m.value).to.be.undefined;
      expect(target.value).to.be.undefined;
    });

    it('should accept a default value', function () {
      parseDefinition({ type: 'date', default: '2014/10/29' });
      var target = { value: undefined };
      manipulate(target);
      manipulator.value.should.be.instanceOf(Date);
      manipulator.value.should.deep.equal(new Date('2014/10/29'));
      target.value.should.equal('2014-10-29');
    });

    it('should accept a computed default value', function () {
      var today = moment().format('YYYY-MM-DD');
      parseDefinition({
        computedExpressions: {
          today: 'new Date()'
        },
        fields: [ { type: 'date', computedDefault: 'today', name: 'd' } ]
      });
      var target = { value: undefined };
      manipulate(target);
      var dateManipulator = manipulator.propertyManipulators.d;
      dateManipulator.value.should.be.instanceOf(Date);
      target.value.d.should.equal(today);
    });

    it('should format dates', function () {
      parseDefinition({ type: 'date', default: '2014/10/29' });
      var target = { value: undefined };
      manipulate(target);
      expect(manipulator.format('YYYY')).to.equal('2014');
      expect(manipulator.format('DD/MM/YYYY')).to.equal('29/10/2014');
    });

    it('should format invalid dates', function () {
      parseDefinition({ type: 'date' });
      var target = { value: undefined };
      manipulate(target);
      expect(manipulator.format('YYYY')).to.equal('');
      expect(manipulator.format('DD/MM/YYYY', 'Invalid date')).to.equal('Invalid date');
    });
  });

  describe('skrBoolean', function () {

    it('should validate booleans', function () {
      parseDefinition({ type: 'boolean' });
      testAssertions(true).should.eql({ boolean: 'ok' });
      testAssertions(false).should.eql({ boolean: 'ok' });
      testAssertions('hello').should.eql({ boolean: 'failed' });
      testAssertions(0).should.eql({ boolean: 'failed' });
      testAssertions(undefined).should.eql({ boolean: 'pending' });
      testAssertions(null).should.eql({ boolean: 'pending' });
    });

    it('should accept "0" and "1" because DDE used those as default values', function () {
      parseDefinition({ type: 'boolean' });
      testAssertions('1').should.eql({ boolean: 'ok' });
      testAssertions('0').should.eql({ boolean: 'ok' });
      testAssertions('2').should.eql({ boolean: 'failed' });
    });

    it('should deal with "0" when manipulating', function () {
      parseDefinition({ type: 'boolean' });
      var target = { value: '0' };
      var m = manipulate(target);
      expect(m.value).to.equal(false);
      expect(m.errors).to.have.lengthOf(0);
    });

    it('should deal with "1" when manipulating', function () {
      parseDefinition({ type: 'boolean' });
      var target = { value: '1' };
      var m = manipulate(target);
      expect(m.value).to.equal(true);
      expect(m.errors).to.have.lengthOf(0);
    });
  });

  describe('skrEmail', function () {

    it('should validate email addresses', function () {
      parseDefinition({ type: 'email' });
      testAssertions('foo.bar@bla.com').should.eql({ email: 'ok' });
      testAssertions('hello').should.eql({ email: 'failed' });
      testAssertions(0).should.eql({ email: 'failed' });
      testAssertions(undefined).should.eql({ email: 'pending' });
      testAssertions(null).should.eql({ email: 'pending' });
      testAssertions('').should.eql({ email: 'pending' });
    });

    it('should not accept the empty string as valid required email', function () {
      parseDefinition({ type: 'email', required: true });
      validate('')
        .should.eql({
          assertions: { email: 'pending', required: 'failed' }
        });
    });
  });

  describe('skrText', function () {

    it('should validate strings', function () {
      parseDefinition({ type: 'text' });
      testAssertions('hello').should.eql({ text: 'ok' });
      testAssertions(0).should.eql({ text: 'failed' });
      testAssertions(undefined).should.eql({ text: 'pending' });
      testAssertions(null).should.eql({ text: 'pending' });
    });

    it('should test a given regular expression', function () {
      parseDefinition({ type: 'text', regexp: /^[A-Za-z0-9]+/ });
      testAssertions('hello').should.eql({ text: 'ok', regexp: 'ok' });
      testAssertions('_@!').should.eql({ text: 'ok', regexp: 'failed' });
      testAssertions(123).should.eql({ text: 'failed', regexp: 'pending' });
      testAssertions(undefined).should.eql({ text: 'pending', regexp: 'pending' });
      testAssertions(null).should.eql({ text: 'pending', regexp: 'pending' });
    });

    it('should reject a malformed regular expression', function () {
      (function () {
        parseDefinition({ type: 'text', regexp: 123 });
      }).should.throw('skrRegexp: expected a regular expression, got 123');
    });
  });

  describe('skrObject', function () {

    it('should validate objects', function () {
      parseDefinition({ /* object is the default type */ });
      testAssertions({}).should.eql({ object: 'ok' });
      testAssertions(42).should.eql({ object: 'failed' });
      testAssertions('hello').should.eql({ object: 'failed' });
      testAssertions(true).should.eql({ object: 'failed' });
    });

    var nameAndAge = {
      fields: [
        { type: 'text', name: 'name', required: true },
        { type: 'number', name: 'age' }
      ]
    };

    it('should handle missing object', function () {
      parseDefinition(nameAndAge);
      validate(null).should.eql({ assertions: { object: 'pending' } });
      validate(undefined).should.eql({ assertions: { object: 'pending' } });
    });

    it('should handle invalid objects', function () {
      parseDefinition(nameAndAge);
      validate(123).should.eql({
        assertions: { object: 'failed' }
      });
    });

    it('should manipulate objects', function () {
      parseDefinition(nameAndAge);
      var target = { value: { name: 'Joe', age: 42 }};
      var m = manipulate(target);
      var ms = m.propertyManipulators;
      expect(ms).to.have.keys('name', 'age');
      ms.name.value.should.equal('Joe');
      ms.age.value.should.equal(42);
      ms.age.value = 43;
      ms.age.value.should.equal(43);
      target.value.age.should.equal(43);
    });

    it('should validate continuously via manipulator', function () {
      parseDefinition(nameAndAge);
      var target = { value: { name: 'Joe', age: 42 }};
      var m = manipulate(target);
      expect(m.assertions).to.deep.equal({ object: 'ok' });
      m.edit(function () { return null; });
      expect(m.assertions).to.deep.equal({ object: 'pending' });
      m.assertions.should.equal(m.assertions);
    });

    it('should list nested manipulators', function () {
      parseDefinition(nameAndAge);
      var target = { value: { name: 'Joe', age: 42 }};
      var m = manipulate(target);
      var ms = m.nestedManipulators;
      ms.should.have.lengthOf(2);
      ms[0].value.should.equal('Joe');
      ms[1].value.should.equal(42);
      ms[1].value = 43;
      ms[1].value.should.equal(43);
      target.value.age.should.equal(43);
    });

    it('should self-initialize', function () {
      parseDefinition(nameAndAge);
      var target = { value: undefined };
      var m = manipulate(target);
      target.value.should.eql({ name: undefined, age: undefined });
      // the following should not raise an error
      expect(m.propertyManipulators.name.value).equal(undefined);
    });

    it('should correctly initialize even in read-only mode', function () {
      parseDefinition(nameAndAge);
      var m = manipulateReadOnly({});
      expect(m.propertyManipulators.name.value).to.be.undefined;
    });

    it('should re-initialize after clearing', function () {
      parseDefinition(nameAndAge);
      var target = { value: { name: 'Joe', age: 42 }};
      var m = manipulate(target);
      m.clear();
      expect(target.value).to.deep.equal({ name: undefined, age: undefined });
    });

    it('should self-initialize properties', function () {
      parseDefinition({
        fields: [
          { type: 'number', name: 'someNumber', default: 42 },
          { type: 'anything', name: 'something' }
        ]
      });
      var target = { value: undefined };
      manipulate(target);
      target.value.should.eql({ someNumber: 42, something: undefined });
    });

    it('should support being inactive', function () {
      parseDefinition({
        type: 'choice',
        choices: [
          {
            name: 'A',
            fields: [
              {
                name: 'nested',
                fields: [ { type: 'number', default: 5, name: 'someNumber' } ]
              }
            ]
          },
          {
            name: 'B'
          }
        ]
      });
      var target = {};
      manipulate(target);
      expect(target.value).to.deep.equal({
        selectedOption: undefined,
        nested: {
          someNumber: undefined
        }
      });
    });

    describe('skrPropertyName', function () {

      it('should require skrObject', function () {
        (function () {
          parseDefinition({
            type: 'number',
            name: 'foo'
          });
        }).should.not.throw();
      });
    });
  });

  describe('skrList', function () {

    it('should validate lists', function () {
      parseDefinition({ type: 'list' });
      testAssertions([]).should.eql({ list: 'ok' });
      testAssertions(42).should.eql({ list: 'failed' });
      testAssertions('hello').should.eql({ list: 'failed' });
      testAssertions(true).should.eql({ list: 'failed' });
    });

    var stamps = {
      type: 'list',
      element: {
        type: 'number'
      }
    };

    it('should handle missing list', function () {
      parseDefinition(stamps);
      validate(null).should.eql({ assertions: { list: 'pending' }});
      validate(undefined).should.eql({ assertions: { list: 'pending' }});
    });

    it('should handle invalid lists', function () {
      parseDefinition(stamps);
      validate(123).should.eql({ assertions: { list: 'failed' }});
    });

    it('should manipulate lists', function () {
      parseDefinition(stamps);
      var list = [ 1, 2, 3 ];
      var m = manipulate({ value: list });
      expect(m.elementManipulators).to.have.lengthOf(3);
      m.elementManipulators[0].value.should.equal(1);
      m.elementManipulators[1].value.should.equal(2);
      m.elementManipulators[2].value.should.equal(3);

      m.addElement(4);
      expect(m.elementManipulators).to.have.lengthOf(4);
      m.elementManipulators[2].value.should.equal(3);
      m.elementManipulators[3].value.should.equal(4);

      m.elementManipulators[1].deleteElement();
      expect(m.elementManipulators).to.have.lengthOf(3);
      list.should.eql([ 1, 3, 4 ]);
      m.elementManipulators[0].value.should.equal(1);
      m.elementManipulators[1].value.should.equal(3);
      m.elementManipulators[2].value.should.equal(4);

      // Important for angular.js to have stable models
      m.elementManipulators.should.equal(m.elementManipulators);
      m.elementManipulators[0].should.equal(m.elementManipulators[0]);
    });

    it('should support objects as list elements', function () {
      parseDefinition({
        type: 'list',
        fields: [ { type: 'number', name: 'someNumber' } ]
      });
      manipulate({ value: undefined });
      expect(manipulator.elementManipulators).to.have.lengthOf(0);
      manipulator.addElement();
      expect(manipulator.elementManipulators).to.have.lengthOf(1);
      manipulator.elementManipulators[0].path.should.deep.equal([ 0 ]);
      manipulator.addElement();
      expect(manipulator.elementManipulators).to.have.lengthOf(2);
      manipulator.elementManipulators[1].path.should.deep.equal([ 1 ]);
    });

    it('should self-initialize', function () {
      parseDefinition(stamps);
      var target = { value: undefined };
      manipulate(target);
      target.value.should.eql([]);
    });

    it('should re-initialize after clearing', function () {
      parseDefinition(stamps);
      var list = [ 1, 2, 3 ];
      var m = manipulate({ value: list });
      m.clear();
      expect(m.read()).to.deep.equal([]);
    });

    it('should support read-only mode', function () {
      parseDefinition(stamps);
      var list = [ 1, 2, 3 ];
      var m = manipulateReadOnly({ value: list });
      expect(m.elementManipulators).to.have.lengthOf(3);

      m.addElement(4);
      expect(m.elementManipulators).to.have.lengthOf(3);
      m.elementManipulators[0].value.should.equal(1);
      m.elementManipulators[1].value.should.equal(2);
      m.elementManipulators[2].value.should.equal(3);

      m.elementManipulators[1].deleteElement();
      expect(m.elementManipulators).to.have.lengthOf(3);
      list.should.eql([ 1, 2, 3 ]);
      m.elementManipulators[0].value.should.equal(1);
      m.elementManipulators[1].value.should.equal(2);
      m.elementManipulators[2].value.should.equal(3);
    });

    it('should correctly initialize even in read-only mode', function () {
      parseDefinition(stamps);
      manipulateReadOnly({});
      expect(manipulator.elementManipulators).to.have.lengthOf(0);
    });

    it('should automatically register element manipulators', function () {
      parseDefinition(stamps);
      var list = [ 1, 2, 3 ];
      manipulate({ value: list });
      expect(document.registeredManipulators).to.have.property('%.0');
      expect(document.registeredManipulators).to.have.property('%.1');
      expect(document.registeredManipulators).to.have.property('%.2');
    });

    it('should support an initial length', function () {
      parseDefinition({
        type: 'list',
        fields: [ { type: 'number', name: 'someNumber' } ],
        initialLength: 2
      });
      var target = {};
      manipulate(target);
      expect(manipulator.elementManipulators).to.have.lengthOf(2);
      expect(target.value).to.deep.equal([
        { someNumber: undefined },
        { someNumber: undefined }
      ]);
    });

    it('should support an initial length combined with onlyWhen', function () {
      parseDefinition({
        type: 'list',
        fields: [ { type: 'number', name: 'someNumber' } ],
        initialLength: 2,
        onlyWhen: 'isListActive',
        computedExpressions: {
          isListActive: 'Boolean($$.isListActive)'
        }
      });
      var target = {};
      manipulate(target);
      expect(manipulator.isActive()).to.be.false;
      expect(manipulator.elementManipulators).to.have.lengthOf(0);
      expect(target.value).to.deep.equal([]);

      manipulator.isListActive = true;
      manipulator.invalidate();
      expect(manipulator.isActive()).to.be.true;
      expect(manipulator.elementManipulators).to.have.lengthOf(2);
    });

    it('should support aggregating computed expressions', function () {
      parseDefinition({
        fields: [
          {
            type: 'list',
            name: 'lijstje',
            element: {
              fields: [ { type: 'text', name: 'tekst' } ],
              computedExpressions: {
                tekstlengte: '$.tekst.length'
              }
            }
          },
          {
            type: 'number',
            name: 'totaal',
            computedWith: 'totaal'
          }
        ],
        computedExpressions: {
          totaal: '$$.propertyManipulators.lijstje.elementManipulators.reduce(' +
            'function (memo, em) { return memo + em.computedExpressions.tekstlengte; }, 0)'
        }
      });
      manipulate({});
      var lijstje = manipulator.propertyManipulators.lijstje;
      lijstje.addElement();
      var el1 = lijstje.elementManipulators[0];
      el1.propertyManipulators.tekst.value = 'abc';
      expect(manipulator.computedExpressions.totaal).to.equal(3);
      lijstje.addElement();
      var el2 = lijstje.elementManipulators[1];
      el2.propertyManipulators.tekst.value = 'xyz-uvw';
      expect(manipulator.computedExpressions.totaal).to.equal(10);
    });

    it('should support nested lists', function () {
      parseDefinition({
        type: 'list',
        fields: [
          {
            type: 'list',
            name: 'nestedList',
            element: { type: 'number' }
          }
        ]
      });
      var target = {};
      manipulate(target);
      expect(manipulator.elementManipulators).to.have.lengthOf(0);
      manipulator.addElement();
      expect(manipulator.elementManipulators).to.have.lengthOf(1);
      manipulator.elementManipulators[0].propertyManipulators.nestedList.addElement();
      expect(target.value).to.deep.equal([ { nestedList: [ undefined ] } ]);

      manipulator.addElement(); // this should not throw
      expect(target.value).to.deep.equal([
        { nestedList: [ undefined ] },
        { nestedList: [] }
      ]);
    });

    it('should combine onlyWhen and initialLength correctly', function () {
      parseDefinition({
        type: 'list',
        initialLength: 2,
        computedExpressions: {
          bla: 'false'
        },
        onlyWhen: 'bla',
        fields: [ // the bug only occurred with more than one field
          {
            type: 'number',
            name: 'someNumberInAList'
          },
          {
            type: 'number',
            name: 'someTextInAList',
            'default': 'hello'
          }
        ]
      });
      var target = {};
      manipulate(target);
      expect(manipulator.isActive()).to.be.false;
      expect(target).to.deep.equal({
        value: []
      });
    });

  });

  describe('skrChoice', function () {

    // var AorB = {
    //   skrChoice: [
    //     {
    //       skrOption: 'A',
    //       label: 'Wat is A?',
    //       nested: {
    //         skrNumber: true,
    //         skrMinimum: 42,
    //         skrPropertyName: 'someNumber'
    //       }
    //     },
    //     {
    //       skrOption: 'B',
    //       skrEmail: true,
    //       skrPropertyName: 'someEmail'
    //     }
    //   ]
    // };

    var AorB = {
      type: 'choice',
      choices: [
        {
          name: 'A',
          label: 'Wat is A?',
          fields: [ { type: 'number', minimum: 42, name: 'someNumber' } ]
        },
        {
          name: 'B',
          fields: [ { type: 'email', name: 'someEmail' } ]
        }
      ]
    };

    it('should validate choices', function () {
      parseDefinition(AorB);

      testAssertions({ selectedOption: 'A' }).should.eql({ choice: 'ok' });
      testAssertions({ selectedOption: 'B' }).should.eql({ choice: 'ok' });
      testAssertions({ selectedOption: 'C' }).should.eql({ choice: 'failed' });
      testAssertions({ selectedOption: undefined }).should.eql({ choice: 'pending' });
      testAssertions({ selectedOption: null }).should.eql({ choice: 'pending' });
      testAssertions({}).should.eql({ choice: 'pending' });
      testAssertions(undefined).should.eql({ choice: 'pending' });
      testAssertions(null).should.eql({ choice: 'pending' });
      testAssertions(42).should.eql({ choice: 'failed' });

      validate({ selectedOption: 'C' }).should.eql({ assertions: { choice: 'failed' }});
      validate({ selectedOption: undefined }).should.eql({ assertions: { choice: 'pending' }});
      validate({ selectedOption: null }).should.eql({ assertions: { choice: 'pending' }});
    });

    it('should store the option labels', function () {
      parseDefinition(AorB);
      expect(description)
        .to.have.deep.property('optionDescriptions.A.label', 'Wat is A?');
    });

    it('should manipulate choices', function () {
      parseDefinition(AorB);
      var target = {
        value: {
          selectedOption: 'A',
          someNumber: 99
        }
      };
      var m = manipulate(target);
      expect(m.selectedOption).to.equal('A');
      expect(m.optionManipulators).to.have.keys('A', 'B');
      expect(m.optionManipulators.A.isSelected).to.be.true;
      expect(m.optionManipulators.B.isSelected).to.be.false;
      expect(m.optionManipulators.A.propertyManipulators).to.have.keys('someNumber');
      expect(m.optionManipulators.B.propertyManipulators).to.have.keys('someEmail');
      expect(m.optionManipulators.A.propertyManipulators.someNumber.value).to.equal(99);
    });

    it('should validate continuously via manipulator', function () {
      parseDefinition(AorB);
      var target = {
        value: {
          selectedOption: undefined
        }
      };
      var m = manipulate(target);
      expect(m.assertions).to.deep.equal({ choice: 'pending' });
      m.selectedOption = 'B';
      expect(m.assertions).to.deep.equal({ choice: 'ok' });
      m.assertions.should.equal(m.assertions);
    });

    it('should not clear nested option properties when configured not to', function () {
      parseDefinition(AorB);
      var target = {
        value: {
          selectedOption: 'A',
          someNumber: 99
        }
      };
      var m = manipulate(target, { clearInactive: false });
      expect(m.selectedOption).to.equal('A');

      // check that we do not lose data
      m.selectedOption = 'A';
      target.value.should.deep.equal({
        selectedOption: 'A',
        someNumber: 99
      });

      // check that properties are not cleared when switching
      m.selectedOption = 'B';
      expect(m.selectedOption).to.equal('B');
      expect(m.optionManipulators.A.isSelected).to.be.false;
      expect(m.optionManipulators.B.isSelected).to.be.true;
      target.value.should.deep.equal({
        selectedOption: 'B',
        someNumber: 99
      });

      // check that the original data is restored when switching back
      m.selectedOption = 'A';
      expect(m.optionManipulators.A.propertyManipulators.someNumber.value).to.equal(99);
    });

    it('should clear nested option properties when unselected', function () {
      parseDefinition(AorB);
      var target = {
        value: {
          selectedOption: 'A',
          someNumber: 99
        }
      };
      var m = manipulate(target);
      expect(m.selectedOption).to.equal('A');

      // check that we do not lose data
      m.selectedOption = 'A';
      target.value.should.deep.equal({
        selectedOption: 'A',
        someNumber: 99
      });

      // check that properties are cleared when switching
      m.selectedOption = 'B';
      expect(m.selectedOption).to.equal('B');
      expect(m.optionManipulators.A.isSelected).to.be.false;
      expect(m.optionManipulators.B.isSelected).to.be.true;
      target.value.should.deep.equal({
        selectedOption: 'B',
        someNumber: undefined
      });
    });

    it('should re-initialize nested option properties after clearing', function () {
      parseDefinition({
        type: 'choice',
        choices: [
          {
            name: 'A',
            fields: [
              {
                name: 'someObject',
                fields: [ { type: 'anything', name: 'anything' } ]
              }
            ]
          },
          {
            name: 'B'
          }
        ]
      });
      var target = {
        value: {
          selectedOption: 'A',
          someObject: { anything: 42 }
        }
      };
      var m = manipulate(target);
      expect(m.selectedOption).to.equal('A');
      expect(m.optionManipulators.A.propertyManipulators.someObject.read())
        .to.deep.equal({ anything: 42 });
      m.selectedOption = 'B';
      expect(m.optionManipulators.A.propertyManipulators.someObject.read())
        .to.deep.equal({ anything: undefined });
    });

    it('should self-initialize', function () {
      parseDefinition(AorB);
      var target = { value: undefined };
      var m = manipulate(target);
      expect(target.value).to.deep.equal({
        selectedOption: undefined
      });
      // the following should not raise an error
      expect(m.selectedOption).equal(undefined);
    });

    it('should re-initialize after clearing', function () {
      parseDefinition(AorB);
      var target = {
        value: {
          selectedOption: 'A',
          someNumber: 99
        }
      };
      var m = manipulate(target);
      m.clear();
      expect(target.value).to.deep.equal({
        selectedOption: undefined,
        someNumber: undefined
      });
    });

    it('should support read-only mode', function () {
      parseDefinition(AorB);
      var target = {
        value: {
          selectedOption: 'A',
          someNumber: 99
        }
      };
      var m = manipulateReadOnly(target);

      m.selectedOption = 'B';
      expect(m.selectedOption).to.equal('A');
      expect(m.optionManipulators.A.isSelected).to.be.true;
      expect(m.optionManipulators.B.isSelected).to.be.false;
      expect(m.optionManipulators.A.propertyManipulators.someNumber.value).to.equal(99);
      m.optionManipulators.A.propertyManipulators.someNumber.value = 42;
      expect(m.optionManipulators.A.propertyManipulators.someNumber.value).to.equal(99);
    });

    it('should correctly initialize even in read-only mode', function () {
      parseDefinition(AorB);
      var m = manipulateReadOnly({});
      expect(m.optionManipulators.A.isSelected).to.be.false;
    });

    it('should refuse nested properties with the same name', function () {
      parseDefinition({
        type: 'choice',
        choices: [
          {
            name: 'A',
            fields: [
              { type: 'anything', name: 'twice' }
            ]
          },
          {
            name: 'B',
            fields: [
              { type: 'anything', name: 'twice' }
            ]
          }
        ]
      });
      expect(function () {
        manipulate({ value: undefined });
      }).to.throw('Path already in use: %.twice');
    });

    it('should mark property manipulators of unselected options as inactive', function () {
      parseDefinition(AorB);
      var target = {
        value: {
          selectedOption: 'A'
        }
      };
      var m = manipulate(target);
      expect(m.isActive()).to.be.true;
      expect(m.optionManipulators.A.propertyManipulators.someNumber.isActive()).to.be.true;
      expect(m.optionManipulators.B.propertyManipulators.someEmail.isActive()).to.be.false;
    });

    it('should require a selected option with skrRequired', function () {
      parseDefinition({
        type: 'choice',
        choices: [
          { name: 'A' },
          { name: 'B' }
        ],
        required: true
      });
      testAssertions({ selectedOption: undefined }).should.deep.equal({
        choice: 'pending',
        required: 'failed'
      });
      testAssertions({ selectedOption: 'A' }).should.deep.equal({
        choice: 'ok',
        required: 'ok'
      });
    });

    it('should accept a default value', function () {
      parseDefinition({
        type: 'choice',
        choices: [
          { name: 'A' },
          { name: 'B' }
        ],
        default: 'B'
      });
      manipulate({});
      expect(manipulator.selectedOption).to.equal('B');
    });

    it('should support computed values', function () {
      parseDefinition({
        fields: [
          {
            type: 'text',
            name: 'textField'
          },
          {
            type: 'choice',
            name: 'choiceField',
            choices: [
              { name: 'A' },
              { name: 'B' }
            ],
            computedWith: 'zot'
          }
        ],
        computedExpressions: {
          zot: '$.textField'
        }
      });
      manipulate({});
      var textFieldManipulator = manipulator.propertyManipulators.textField;
      var choiceFieldManipulator = manipulator.propertyManipulators.choiceField;
      expect(choiceFieldManipulator.selectedOption).to.be.undefined;
      textFieldManipulator.value = 'A';
      expect(choiceFieldManipulator.selectedOption).to.equal('A');
      textFieldManipulator.value = 'B';
      expect(choiceFieldManipulator.selectedOption).to.equal('B');
      textFieldManipulator.value = 'X';
      expect(choiceFieldManipulator.selectedOption).to.be.undefined;
    });

    describe('with a default value and skrOnlyWhen', function () {

      var onlyWhenWithDefault = {
        computedExpressions: {
          someBooleanIsTrue: '$.nested.someBoolean === true'
        },
        fields: [
          {
            fields: [ { type: 'boolean', name: 'someBoolean' } ],
            name: 'nested'
          },
          {
            type: 'choice',
            name: 'someChoice',
            choices: [ { name: 'A' }, { name: 'B' } ],
            required: true,
            onlyWhen: 'someBooleanIsTrue',
            default: 'B'
          }
        ]
      };

      var obj, someChoice, nested, someBoolean;

      beforeEach(function () {
        parseDefinition(onlyWhenWithDefault);
        obj = {
          nested: { someBoolean: false }
        };
        manipulate({ value: obj });
        someChoice = manipulator.propertyManipulators.someChoice;
        nested = manipulator.propertyManipulators.nested;
        someBoolean = nested.propertyManipulators.someBoolean;
      });

      it('should not initialize with the default value if the condition is not met', function () {
        expect(manipulator.computedExpressions.someBooleanIsTrue).to.be.false;
        expect(someChoice.isActive()).to.be.false;
        expect(someChoice.selectedOption).to.be.undefined;
      });

      it('should apply the default value when the condition becomes true', function () {
        someBoolean.value = true;
        expect(someChoice.isActive()).to.be.true;
        expect(someChoice.selectedOption).to.equal('B');
      });
    });
  });

  describe('skrMultiChoice', function () {

    var AorB = {
      type: 'multichoice',
      choices: [
        {
          name: 'A',
          fields: [
            {
              type: 'number',
              name: 'someNumber',
              minimum: 42
            }
          ]
        },
        {
          name: 'B',
          fields: [
            {
              type: 'email',
              name: 'someEmail'
            }
          ]
        }
      ]
    };

    it('should validate multichoices', function () {
      parseDefinition(AorB);

      testAssertions({ selectedOptions: {} }).should.eql({ multichoice: 'ok' });
      testAssertions({ selectedOptions: { A: true } })
        .should.eql({ multichoice: 'ok' });
      testAssertions({ selectedOptions: { B: false } })
        .should.eql({ multichoice: 'ok' });
      testAssertions({ selectedOptions: { A: true, B: true } })
        .should.eql({ multichoice: 'ok' });
      testAssertions({ selectedOptions: { B: true, A: false } })
        .should.eql({ multichoice: 'ok' });
      testAssertions({ selectedOptions: { C: true } })
        .should.eql({ multichoice: 'failed' });
      testAssertions({ selectedOptions: undefined })
        .should.eql({ multichoice: 'pending' });
      testAssertions({ selectedOptions: null })
        .should.eql({ multichoice: 'pending' });
      testAssertions({}).should.eql({ multichoice: 'pending' });
      testAssertions(undefined).should.eql({ multichoice: 'pending' });
      testAssertions(null).should.eql({ multichoice: 'pending' });
    });

    it('should manipulate multichoices', function () {
      parseDefinition(AorB);
      var target = {
        value: {
          selectedOptions: { A: true, B: true },
          someNumber: 42
        }
      };
      var m = manipulate(target);
      expect(m.optionManipulators).to.have.keys('A', 'B');
      expect(m.optionManipulators.A.isSelected).to.be.true;
      expect(m.optionManipulators.B.isSelected).to.be.true;
      expect(m.optionManipulators.A.propertyManipulators.someNumber.value).to.equal(42);
    });

    it('should clear nested option properties when unselected', function () {
      parseDefinition(AorB);
      var target = {
        value: {
          selectedOptions: { A: true, B: true },
          someNumber: 42
        }
      };
      var m = manipulate(target);

      // check that we do not lose data
      m.optionManipulators.A.isSelected = true;
      target.value.should.eql({
        selectedOptions: { A: true, B: true },
        someNumber: 42
      });

      m.optionManipulators.A.isSelected = false;
      expect(m.optionManipulators.A.isSelected).to.be.false;
      target.value.should.eql({
        selectedOptions: { A: false, B: true },
        someNumber: undefined
      });
    });

    it('should re-initialize nested option properties after clearing', function () {
      parseDefinition({
        type: 'multichoice',
        choices: [
          {
            name: 'A',
            fields: [
              {
                fields: [ { type: 'anything', name: 'anything' } ],
                name: 'someObject'
              }
            ]
          },
          { name: 'B' }
        ]
      });
      var target = {
        value: {
          selectedOptions: { A: true, B: true },
          someObject: { anything: 42 }
        }
      };
      var m = manipulate(target);
      expect(m.optionManipulators.A.isSelected).to.be.true;
      expect(m.optionManipulators.A.propertyManipulators.someObject.read())
        .to.deep.equal({ anything: 42 });
      m.optionManipulators.A.isSelected = false;
      expect(m.optionManipulators.A.propertyManipulators.someObject.read())
        .to.deep.equal({ anything: undefined });
    });

    it('should self-initialize', function () {
      parseDefinition(AorB);
      var target = { value: undefined };
      var m = manipulate(target);
      target.value.should.eql({
        selectedOptions: {
          A: false,
          B: false
        }
      });
      // the following should not raise an error
      expect(m.optionManipulators.A.isSelected).to.be.false;
      expect(m.optionManipulators.B.isSelected).to.be.false;
      expect(m.optionManipulators.A.propertyManipulators.someNumber.value).to.be.undefined;
      expect(m.optionManipulators.B.propertyManipulators.someEmail.value).to.be.undefined;
    });

    it('should re-initialize after clearing', function () {
      parseDefinition(AorB);
      var target = {
        value: {
          selectedOptions: { A: true, B: true },
          someNumber: 42
        }
      };
      var m = manipulate(target);
      m.clear();
      expect(target.value).to.deep.equal({
        selectedOptions: { A: false, B: false },
        someNumber: undefined
      });
    });

    it('should support read-only mode', function () {
      parseDefinition(AorB);
      var target = {
        value: {
          selectedOptions: { A: true, B: true },
          someNumber: 99
        }
      };
      var m = manipulateReadOnly(target);

      m.optionManipulators.A.isSelected = false;
      expect(m.optionManipulators.A.isSelected).to.be.true;
      target.value.should.eql({
        selectedOptions: { A: true, B: true },
        someNumber: 99
      });

      expect(m.optionManipulators.A.propertyManipulators.someNumber.value).to.equal(99);
      m.optionManipulators.A.propertyManipulators.someNumber.value = 42;
      expect(m.optionManipulators.A.propertyManipulators.someNumber.value).to.equal(99);
    });

    it('should correctly initialize even in read-only mode', function () {
      parseDefinition(AorB);
      var m = manipulateReadOnly({});
      expect(m.optionManipulators.A.isSelected).to.be.false;
    });

    it('should mark property manipulators of unselected options as inactive', function () {
      parseDefinition(AorB);
      var target = {
        value: {
          selectedOptions: { A: true, B: false }
        }
      };
      var m = manipulate(target);
      expect(m.isActive()).to.be.true;
      expect(m.optionManipulators.A.propertyManipulators.someNumber.isActive()).to.be.true;
      expect(m.optionManipulators.B.propertyManipulators.someEmail.isActive()).to.be.false;
    });

    it('should accept default values', function () {
      parseDefinition({
        type: 'multichoice',
        choices: [
          { name: 'A', default: true },
          { name: 'B', default: false }
        ]
      });
      manipulate({});
      expect(manipulator.optionManipulators.A.isSelected).to.be.true;
      expect(manipulator.optionManipulators.B.isSelected).to.be.false;
    });

    it('should support computed values', function () {
      parseDefinition({
        fields: [
          { type: 'number', name: 'numberField' },
          {
            type: 'multichoice',
            name: 'multichoiceField',
            choices: [ { name: 'A' }, { name: 'B' } ],
            computedWith: 'zot'
          }
        ],
        computedExpressions: {
          zot: '{ A: $.numberField === 42 }'
        }
      });
      manipulate({});
      var numberFieldManipulator = manipulator.propertyManipulators.numberField;
      var multichoiceFieldManipulator = manipulator.propertyManipulators.multichoiceField;
      expect(multichoiceFieldManipulator.selectedOptions).to.be.undefined;
      numberFieldManipulator.value = 42;
      expect(multichoiceFieldManipulator.optionManipulators.A.isSelected).to.be.true;
      expect(multichoiceFieldManipulator.optionManipulators.B.isSelected).to.be.false;
      numberFieldManipulator.value = 101;
      expect(multichoiceFieldManipulator.optionManipulators.A.isSelected).to.be.false;
      expect(multichoiceFieldManipulator.optionManipulators.B.isSelected).to.be.false;
      numberFieldManipulator.value = 42;
      multichoiceFieldManipulator.optionManipulators.B.isSelected = true;
      expect(multichoiceFieldManipulator.optionManipulators.A.isSelected).to.be.true;
      expect(multichoiceFieldManipulator.optionManipulators.B.isSelected).to.be.true;
    });

    describe('with a default value and skrOnlyWhen', function () {

      var onlyWhenWithDefault = {
        computedExpressions: {
          someBooleanIsTrue: '$.nested.someBoolean === true'
        },
        fields: [
          {
            fields: [ { type: 'boolean', name: 'someBoolean' } ],
            name: 'nested'
          },
          {
            type: 'multichoice',
            name: 'someChoice',
            choices: [ { name: 'A', default: true }, { name: 'B', default: false } ],
            required: true,
            onlyWhen: 'someBooleanIsTrue'
          }
        ]
      };

      var obj, someChoice, nested, someBoolean;

      beforeEach(function () {
        parseDefinition(onlyWhenWithDefault);
        obj = {
          nested: { someBoolean: false }
        };
        manipulate({ value: obj });
        someChoice = manipulator.propertyManipulators.someChoice;
        nested = manipulator.propertyManipulators.nested;
        someBoolean = nested.propertyManipulators.someBoolean;
      });

      it('should not initialize with the default value if the condition is not met', function () {
        expect(manipulator.computedExpressions.someBooleanIsTrue).to.be.false;
        expect(someChoice.isActive()).to.be.false;
        expect(someChoice.optionManipulators.A.isSelected).to.be.false;
      });

      it('should apply the default value when the condition becomes true', function () {
        someBoolean.value = true;
        expect(someChoice.isActive()).to.be.true;
        expect(someChoice.optionManipulators.A.isSelected).to.be.true;
      });
    });

    it('should not crash on bad document data', function () {
      parseDefinition({
        fields: [
          {
            type: 'multichoice',
            name: 'foo',
            choices: [
              { name: 'A', fields: [ { type: 'text', name: 'explain' } ] },
              { name: 'B' }
            ]
          }
        ]
      });
      manipulate({ value: { foo: {}}});
    });
  });

  describe('skrCondition', function () {

    it('should test an arbitrary expression', function () {
      parseDefinition({
        type: 'anything',
        conditions: [
          {
            name: 'oneAndTwo',
            expression: '1 > 2'
          }
        ]
      });
      testAssertions(null).should.deep.equal({ oneAndTwo: 'failed' });
    });

    it('should expose current value to the expression as variable "$"', function () {
      parseDefinition({
        type: 'anything',
        conditions: [
          {
            name: 'lessThenTen',
            expression: '$ < 10'
          }
        ]
      });
      testAssertions(9).should.deep.equal({ lessThenTen: 'ok' });
      testAssertions(10).should.deep.equal({ lessThenTen: 'failed' });
    });

    it('should validate continuously via manipulator', function () {
      parseDefinition({
        type: 'anything',
        conditions: [
          {
            name: 'lessThenTen',
            expression: '$ < 10'
          }
        ]
      });
      manipulate({ value: 12 });
      manipulator.assertions.should.deep.equal({ lessThenTen: 'failed' });
      manipulator.edit(function () { return 9; });
      manipulator.assertions.should.deep.equal({ lessThenTen: 'ok' });
    });

    it('should validate continuously with changing properties', function () {
      parseDefinition({
        fields: [
          { type: 'number', name: 'a' },
          { type: 'number', name: 'b' }
        ],
        conditions: [
          {
            name: 'aLessThanB',
            expression: '$.a < $.b'
          }
        ]
      });
      manipulate({ value: { a: 11, b: 10 } });
      manipulator.assertions.should.deep.equal({ object: 'ok', aLessThanB: 'failed' });
      manipulator.propertyManipulators.a.value = 9;
      manipulator.assertions.should.deep.equal({ object: 'ok', aLessThanB: 'ok' });
    });

    it('should require at least one selected option with skrRequired', function () {
      parseDefinition({
        type: 'multichoice',
        choices: [ { name: 'A' }, { name: 'B' } ],
        required: true
      });
      testAssertions({ selectedOptions: {} }).should.deep.equal({
        multichoice: 'ok',
        required: 'failed'
      });
      testAssertions({ selectedOptions: { A: true } }).should.deep.equal({
        multichoice: 'ok',
        required: 'ok'
      });
    });
  });

  describe('skrWarning', function () {
    it('should differentiate between errors and warnings', function () {
      parseDefinition({
        type: 'number',
        minimum: 10,
        validators: [ { maximum: 42, level: 'warning' } ]
      });
      manipulate({ value: 9 });
      expect(manipulator.errors).to.deep.equal([ 'minimum' ]);
      expect(manipulator.warnings).to.deep.equal([]);
      manipulator.value = 42;
      expect(manipulator.errors).to.deep.equal([]);
      expect(manipulator.warnings).to.deep.equal([]);
      manipulator.value = 43;
      expect(manipulator.errors).to.deep.equal([]);
      expect(manipulator.warnings).to.deep.equal([ 'maximum' ]);
      expect(manipulator.warnings).to.equal(manipulator.warnings);
    });

    it('should support ignoring warnings', function () {
      parseDefinition({
        type: 'number',
        minimum: 10,
        validators: [ { maximum: 42, level: 'warning' } ]
      });
      var target = { value: 43 };
      manipulate(target);
      expect(manipulator.warnings).to.deep.equal([ 'maximum' ]);
      manipulator.ignoreWarning('maximum');
      expect(manipulator.warnings).to.deep.equal([]);

      expect(target.ignoredWarnings).to.deep.equal({
        '%': [ 'maximum' ]
      });

      // simulate opening the document again to test if
      // the ignored warnng is persisted
      manipulate(target);
      expect(manipulator.value).to.equal(43);
      expect(manipulator.warnings).to.deep.equal([]);
    });
  });

  describe('skrOnlyWhen', function () {

    var onlyWhen = {
      fields: [
        {
          name: 'nested',
          fields: [ { type: 'boolean', name: 'someBoolean' } ]
        },
        {
          type: 'number',
          name: 'someNumber',
          required: true,
          onlyWhen: 'someBooleanIsTrue'
        }
      ],
      computedExpressions: {
        someBooleanIsTrue: '$.nested.someBoolean === true'
      }
    };

    var someNumber, someBoolean, nested;
    var obj;

    beforeEach(function () {
      parseDefinition(onlyWhen);
      obj = {
        nested: { someBoolean: true },
        someNumber: 3
      };
      manipulate({ value: obj });
      someNumber = manipulator.propertyManipulators.someNumber;
      nested = manipulator.propertyManipulators.nested;
      someBoolean = nested.propertyManipulators.someBoolean;
    });

    it('should evaluate the computed expression', function () {
      // TODO this test should move to a separate describe() block
      // for 'skrComputedExpression'
      expect(manipulator.computedExpressions.someBooleanIsTrue).to.be.true;
      someBoolean.value = false;
      expect(manipulator.computedExpressions.someBooleanIsTrue).to.be.false;
      someBoolean.value = true;
      expect(manipulator.computedExpressions.someBooleanIsTrue).to.be.true;
    });

    it('should only activate a manipulator when the condition is met', function () {
      expect(someNumber.isActive()).to.be.true;
      someBoolean.value = false;
      expect(someNumber.isActive()).to.be.false;
      someBoolean.value = true;
      expect(someNumber.isActive()).to.be.true;
    });

    it('should clear a manipulator when deactived', function () {
      expect(someNumber.value).to.equal(3);
      someBoolean.value = false;
      expect(someNumber.value).to.equal(undefined);
      someBoolean.value = true;
      expect(someNumber.value).to.equal(undefined);
    });

    it('should make a field active when the condition does not produce a boolean', function () {
      // by setting nested to a non-object, we can let the computed expression fail
      nested.edit(function () { return null; });
      expect(function () { return someBoolean.value; }).to.throw();
      expect(manipulator.computedExpressions.someBooleanIsTrue).to.be.undefined;
      expect(someNumber.isActive()).to.be.true;
    });

    describe('with a default value', function () {

      var onlyWhenWithDefault = {
        fields: [
          {
            name: 'nested',
            fields: [ { type: 'boolean', name: 'someBoolean' } ]
          },
          {
            type: 'number',
            name: 'someNumber',
            required: true,
            onlyWhen: 'someBooleanIsTrue',
            default: 101
          }
        ],
        computedExpressions: {
          someBooleanIsTrue: '$.nested.someBoolean === true'
        }
      };

      beforeEach(function () {
        parseDefinition(onlyWhenWithDefault);
        obj = {
          nested: { someBoolean: false }
        };
        manipulate({ value: obj });
        someNumber = manipulator.propertyManipulators.someNumber;
        nested = manipulator.propertyManipulators.nested;
        someBoolean = nested.propertyManipulators.someBoolean;
      });

      it('should not initialize with the default value if the condition is not met', function () {
        expect(someNumber.value).to.be.undefined;
        expect(obj).to.have.property('someNumber', undefined);
      });

      it('should apply the default value when the condition becomes true', function () {
        console.log('wasActive: ', manipulator.wasActive);
        someBoolean.value = true;

        // we need to call isActive() to trigger the reinitialization
        expect(someNumber.isActive()).to.be.true;

        expect(someNumber.value).to.equal(101);
        expect(obj).to.have.property('someNumber', 101);
      });
    });
  });

  describe('skrReadOnly', function () {

    it('should be configurable', function () {
      parseDefinition({
        type: 'number',
        default: 42,
        'read-only': false
      });
      manipulate({ value: undefined });
      expect(manipulator.value).to.equal(42);
      manipulator.value = 101;
      expect(manipulator.value).to.equal(101);
    });

    it('should make a field read-only', function () {
      parseDefinition({
        type: 'number',
        default: 42,
        'read-only': true
      });
      manipulate({ value: undefined });
      expect(manipulator.value).to.equal(42);
      manipulator.value = 101;
      expect(manipulator.value).to.equal(42);
    });
  });

  describe('skrDefault', function () {

    it('should not act in read-only documents', function () {
      parseDefinition({
        type: 'number',
        default: 42
      });
      manipulateReadOnly({ value: undefined });
      expect(manipulator.value).to.be.undefined;
      manipulator.value = 101;
      expect(manipulator.value).to.be.undefined;
    });
  });

  describe('skrComputed', function () {

    describe('with an increment expression', function () {
      var computed = {
        fields: [
          { type: 'number', name: 'someNumber' },
          {
            type: 'number',
            name: 'someOtherNumber',
            computedWith: 'someNumberPlusOne',
            toggleComputed: true
          },
          {
            type: 'number',
            name: 'yetAnotherNumber',
            'read-only': true,
            computedWith: 'someNumberPlusOne'
          },
          {
            type: 'number',
            name: 'thirdNumber',
            computedWith: 'someNumberPlusOne',
            computedWhen: 'someNumberNot42'
          }
        ],
        computedExpressions: {
          someNumberNot42: '$.someNumber !== 42',
          someNumberPlusOne: 'typeof $.someNumber === "number" ? $.someNumber + 1 : undefined'
        }
      };

      var someNumber, someOtherNumber, yetAnotherNumber, thirdNumber;
      var obj, target;

      beforeEach(function () {
        parseDefinition(computed);
        obj = {
          someNumber: 3
        };
        target = { value: obj };
        manipulate(target);
        someNumber = manipulator.propertyManipulators.someNumber;
        someOtherNumber = manipulator.propertyManipulators.someOtherNumber;
        yetAnotherNumber = manipulator.propertyManipulators.yetAnotherNumber;
        thirdNumber = manipulator.propertyManipulators.thirdNumber;
      });

      it('should know whether toggling is supported for computed fields', function () {
        expect(someOtherNumber).to.have.property('toggleComputed', true);
        expect(yetAnotherNumber).to.have.property('toggleComputed', false);
        expect(thirdNumber).to.have.property('toggleComputed', false);
      });

      it('should correctly initialize the computed value', function () {
        expect(someOtherNumber.value).to.equal(4);
      });

      it('should update a field according to an expression', function () {
        someNumber.value = 42;
        expect(someOtherNumber.value).to.equal(43);
      });

      it('should by default accept computed values', function () {
        expect(someOtherNumber.acceptsComputedValues).to.be.true;
      });

      it('should ignore manual input when accepting computed values', function () {
        someOtherNumber.value = 101;
        expect(someOtherNumber.value).to.equal(4);
      });

      it('should support manual input when refusing computed values', function () {
        someOtherNumber.acceptsComputedValues = false;
        someOtherNumber.value = 101;
        expect(someOtherNumber.value).to.equal(101);
      });

      it('should retain manual input when a new computed arrives', function () {
        someOtherNumber.acceptsComputedValues = false;
        someOtherNumber.value = 101;
        someNumber.value = 42;
        expect(someOtherNumber.value).to.equal(101);
      });

      it('should immediately adopt computed value when re-accepting', function () {
        someOtherNumber.acceptsComputedValues = false;
        someOtherNumber.value = 101;
        expect(someOtherNumber.value).to.equal(101);
        someOtherNumber.acceptsComputedValues = true;
        expect(someOtherNumber.value).to.equal(4);
      });

      it('should remember when manual input was provided', function () {
        someOtherNumber.acceptsComputedValues = false;
        someOtherNumber.value = 101;

        // reopen the same document data
        manipulate(target);
        someOtherNumber = manipulator.propertyManipulators.someOtherNumber;
        expect(someOtherNumber.value).to.equal(101);
        expect(someOtherNumber.acceptsComputedValues).to.be.false;
      });

      it('should support read-only fields', function () {
        expect(yetAnotherNumber.value).to.equal(4);
        someNumber.value = 42;
        expect(yetAnotherNumber.value).to.equal(43);
      });

      it('should not modify fields when the entire document is read-only', function () {
        manipulateReadOnly({ value: { someNumber: 42 }});
        someOtherNumber = manipulator.propertyManipulators.someOtherNumber;
        expect(someOtherNumber.value).to.be.undefined;
      });

      it('should allow manual input based on a computed expression', function () {
        expect(thirdNumber.value).to.equal(4);
        someNumber.value = 4;
        thirdNumber.value = 432; // should be ingored
        expect(thirdNumber.value).to.equal(5);
        someNumber.value = 42; // this value should turn of the automatic computation
        thirdNumber.value = 432; // should be allowed now
        expect(thirdNumber.value).to.equal(432);
      });
    });

    it('should handle non-idempotent computed expressions', function () {
      parseDefinition({
        fields: [
          { type: 'date', name: 'now', computedWith: 'now' }
        ],
        computedExpressions: {
          now: 'new Date()'
        }
      });
      manipulate({ value: {} });
      expect(manipulator.propertyManipulators.now.value).to.be.a('date');
    });

    it('should handle mutually recursive computed expressions', function () {
      parseDefinition({
        fields: [
          { type: 'number', name: 'someNumber', computedWith: 'someOtherNumberPlusOne' },
          { type: 'number', name: 'someOtherNumber', computedWith: 'someNumberPlusOne' }
        ],
        computedExpressions: {
          someNumberPlusOne: 'typeof $.someNumber === "number" ? $.someNumber + 1 : 42',
          someOtherNumberPlusOne: 'typeof $.someOtherNumber === "number" ? ' +
            '$.someOtherNumber + 1 : 0'
        }
      });
      var obj = {};
      manipulate({ value: obj });
      // These results are arbitrary and subject to race-conditions.
      expect(obj).to.deep.equal({
        someNumber: 2,
        someOtherNumber: 1
      });
      expect(function () {
        manipulator.invalidate();
      }).to.throw('detected a loop during invalidation');
    });
  });

  describe('descriptions', function () {

    var behaviour, descr;

    beforeEach(function () {
      behaviour = {};
      descr = makeDescription(behaviour);
    });

    it('should pass along data and manipulator to absent predicates', function () {
      var pred = sinon.stub().returns(true);
      descr.absentPredicates.push(pred);
      var manipulator = {};
      descr.isAbsent(5, manipulator);
      expect(pred).to.have.been.calledOnce;
      expect(pred).to.have.been.calledWithExactly(5, manipulator);
    });

    it('should pass along data and manipulator to empty predicates', function () {
      var pred = sinon.stub().returns(true);
      descr.emptyPredicates.push(pred);
      var manipulator = {};
      descr.isEmpty(5, manipulator);
      expect(pred).to.have.been.calledOnce;
      expect(pred).to.have.been.calledWithExactly(5, manipulator);
    });

  });

  describe('manipulators', function () {

    it('should support read-only mode', function () {
      parseDefinition({ type: 'number' });
      var target = { value: 42 };
      var m = manipulateReadOnly(target);
      m.value.should.equal(42);
      m.value = 0;
      m.value.should.equal(42);
    });

    it('should support comments', function () {
      var target = { value: 42 };
      parseDefinition({ type: 'number' });
      var m = manipulate(target);
      m.addComment('This is a comment');
      m.addComment('This is another comment');
      expect(target.comments).to.deep.equal({
        '%': [
          'This is a comment',
          'This is another comment'
        ]
      });
      expect(m.comments).to.eql([
        'This is a comment',
        'This is another comment'
      ]);
      m.deleteComment(0);
      expect(m.comments).to.eql([
        'This is another comment'
      ]);
    });
  });

  describe('documents', function () {

    beforeEach(function () {
      parseDefinition({
        fields: [
          { type: 'number', name: 'someNumber', required: true },
          { type: 'email', name: 'someEmail' }
        ]
      });
      manipulate({ value: undefined });
    });

    it('should continuously validate and measure progress', function () {
      expect(document.errors).to.deep.equal([
        { assertion: 'required', manipulator: manipulator.propertyManipulators.someNumber }
        ]);
      expect(document.warnings).to.deep.equal([]);
      expect(document.progress).to.deep.equal({
        valid: 0,
        invalid: 1,
        optional: 1,
        total: 2,
        validRatio: 0 / 2,
        invalidRatio: 1 / 2,
        optionalRatio: 1 / 2
      });
      manipulator.propertyManipulators.someNumber.value = 42;
      expect(document.errors).to.deep.equal([]);
      expect(document.progress).to.deep.equal({
        valid: 1,
        invalid: 0,
        optional: 1,
        total: 2,
        validRatio: 1 / 2,
        invalidRatio: 0 / 2,
        optionalRatio: 1 / 2
      });
      manipulator.propertyManipulators.someEmail.value = 'abc@xyz.io';
      expect(document.errors).to.deep.equal([]);
      expect(document.progress).to.deep.equal({
        valid: 2,
        invalid: 0,
        optional: 0,
        total: 2,
        validRatio: 2 / 2,
        invalidRatio: 0 / 2,
        optionalRatio: 0 / 2
      });
      expect(document.errors).to.equal(document.errors);
      expect(document.progress).to.equal(document.progress);
    });

    it('should list all comments', function () {
      expect(document.comments).to.deep.equal([]);
      manipulator.propertyManipulators.someEmail.addComment('bla');
      expect(document.comments).to.deep.equal([ {
        comment: 'bla',
        manipulator: manipulator.propertyManipulators.someEmail
      } ]);
      manipulator.propertyManipulators.someEmail.deleteComment(0);
      expect(document.comments).to.deep.equal([]);
    });

    it('should pass provided context to assertions', function () {
      parseDefinition({
        conditions: [
          {
            name: 'lessThenCtx',
            expression: '$.zot < ctx'
          }
        ],
        fields: [ { type: 'number', name: 'zot' } ]
      });
      manipulate({ value: { zot: 9 } }, {
        computedExpressionArguments: _.extend(
          helpers.defaultComputedExpressionArguments(), {
            ctx: function () { return 42; }
          })
      });
      expect(manipulator.errors).to.deep.equal([]);
      manipulator.propertyManipulators.zot.value = 101;
      expect(manipulator.errors).to.deep.equal([ 'lessThenCtx' ]);
    });

    it('should pass provided context to computed expressions', function () {
      parseDefinition({
        computedExpressions: {
          plusCtx: '$.zot + ctx'
        },
        fields: [ { type: 'number', name: 'zot' } ]
      });
      manipulate({ value: { zot: 9 } }, {
        computedExpressionArguments: _.extend(
          helpers.defaultComputedExpressionArguments(), {
            ctx: function () { return 42; }
          })
      });
      expect(manipulator.computedExpressions.plusCtx).to.equal(51);
      manipulator.propertyManipulators.zot.value = 101;
      expect(manipulator.computedExpressions.plusCtx).to.equal(143);
    });

    it('should track dirty state', function () {
      document.isDirty = false;
      manipulator.propertyManipulators.someNumber.value = 42;
      expect(document.isDirty).to.be.true;
    });

    it('should refresh the document errors when deleting items from a list', function () {
      parseDefinition({
        type: 'list',
        fields: [ { type: 'text', name: 'tekstje', required: true } ]
      });
      var target = {};
      manipulate(target);
      manipulator.addElement();
      manipulator.addElement();
      expect(document.errors).to.have.lengthOf(2);
      manipulator.elementManipulators[1].deleteElement();
      expect(document.errors).to.have.lengthOf(1);
      manipulator.elementManipulators[0].deleteElement();
      expect(document.errors).to.have.lengthOf(0);
    });
  });

  var envelope = {
    fields: [
      { type: 'number', name: 'number' },
      { type: 'number', minimum: 1000, maximum: 9999, name: 'zipcode' },
      { type: 'list', element: { type: 'number' }, name: 'stamps' }
    ]
  };

  it('should self-initialize envlopes', function () {
    parseDefinition(envelope);
    var target = { value: undefined };
    manipulate(target);
    target.value.should.eql({
      number: undefined,
      zipcode: undefined,
      stamps: []
    });
  });

});
