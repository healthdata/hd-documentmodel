/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var _ = require('underscore');

var traversalHelper = require('../lib/traversalHelper');

describe('traversalHelper', function () {

  var params, resolve, obtainNestedNodeLocations;
  var someType, someOtherType, unknownType;
  var someContext, someOtherContext, defaultContext;
  var someResolvedType;
  var UNRESOLVED, OK;

  beforeEach(function () {
    someType = 'someType';
    someOtherType = 'someOtherType';
    unknownType = 'unknownType';
    someContext = 'someContext';
    someOtherContext = 'someOtherContext';
    defaultContext = 'defaultContext';
    someResolvedType = 'someResolvedType';

    resolve = sinon.stub();
    obtainNestedNodeLocations = sinon.stub();

    params = {
      initialContext: defaultContext,
      resolve: function (input, context) {
        // sinon can only match calls with simple arguments,
        // so for these tests, we only resolve types using
        // the 'type' property (and the context)
        return resolve(input.type, context);
      },
      obtainNested: function (resolved, input) {
        // for these tests, we always look for nested nodes using
        // the information stored in obtainNestedNodeLocations
        var locations = obtainNestedNodeLocations(resolved);
        return _.mapObject(locations, function (nestedContext, propertyName) {
          return {
            context: nestedContext,
            nested: input[propertyName]
          };
        });
      }
    };

    UNRESOLVED = traversalHelper.UNRESOLVED;
    OK = traversalHelper.OK;
  });

  function testIt(docdef, options, expectedResult) {
    // the 'options' argument is optional, so we need to rearrange the arguments
    // if it is missing
    if (typeof expectedResult === 'undefined') {
      expectedResult = options;
      options = {};
    }
    // the actual params we will use combine the global params and the options
    var ps = _.extend({}, params, options);

    expect(traversalHelper(docdef, ps))
      .to.deep.equal(expectedResult);
  }

  function declareType(type, context, resolvedType, nestedNodeLocations) {
    nestedNodeLocations = nestedNodeLocations || {};
    resolve
      .withArgs(type, context)
      .returns(resolvedType);
    obtainNestedNodeLocations
      .withArgs(resolvedType)
      .returns(nestedNodeLocations);
  }

  it('should return an UNRESOLVED result when no valid type is present', function () {
    var docdef = { type: unknownType };
    testIt(docdef, {
      state: UNRESOLVED,
      input: docdef,
      context: defaultContext
    });
  });

  it('should preserve the entire input in the case of an unrecognized type', function () {
    var docdef = { type: unknownType, extraProperty: 42 };
    testIt(docdef, {
      state: UNRESOLVED,
      input: docdef,
      context: defaultContext
    });
  });

  it('should resolve types via params.resolve()', function () {
    var docdef = { type: someType };

    declareType(someType, defaultContext, someResolvedType);

    testIt(docdef, {
      state: OK,
      resolved: someResolvedType,
      context: defaultContext,
      input: docdef,
      nested: {}
    });
  });

  it('should resolve missing types via the provided context', function () {
    var docdef = {};

    declareType(undefined, defaultContext, someResolvedType);

    testIt(docdef, {
      state: OK,
      resolved: someResolvedType,
      context: defaultContext,
      input: docdef,
      nested: {}
    });
  });

  it('should use the provided initial context', function () {
    var docdef = { type: someType };

    declareType(someType, someContext, someResolvedType);

    testIt(docdef, { initialContext: someContext }, {
      state: OK,
      resolved: someResolvedType,
      context: someContext,
      input: docdef,
      nested: {}
    });
  });

  it('should use the default initial context of the params', function () {
    var docdef = { type: someType };

    declareType(someType, defaultContext, someResolvedType);

    testIt(docdef, {
      state: OK,
      resolved: someResolvedType,
      context: defaultContext,
      input: docdef,
      nested: {}
    });
  });

  it('should give precedence to the custom initial context', function () {
    var docdef = { type: someType };

    declareType(someType, someOtherContext, someResolvedType);

    testIt(docdef, { initialContext: someOtherContext }, {
      state: OK,
      resolved: someResolvedType,
      context: someOtherContext,
      input: docdef,
      nested: {}
    });
  });

  it('should preserve additional properties on the input', function () {
    var docdef = { type: someType, extraProperty: 42 };

    declareType(someType, defaultContext, someResolvedType, {});

    testIt(docdef, {
      state: OK,
      resolved: someResolvedType,
      context: defaultContext,
      input: docdef,
      nested: {}
    });
  });

  it('should process nested properties', function () {
    var docdef = { type: someType, foos: [ { a: 1 }, { b: 2 } ] };

    declareType(someType, defaultContext, someResolvedType, { foos: null });

    testIt(docdef, {
      state: 'OK',
      context: defaultContext,
      input: docdef,
      resolved: someResolvedType,
      nested: {
        foos: [
          {
            state: UNRESOLVED,
            context: null,
            input: docdef.foos[0]
          },
          {
            state: UNRESOLVED,
            context: null,
            input: docdef.foos[1]
          }
        ]
      }
    });
  });

  it('should apply the context for the nested properties', function () {
    var docdef = { type: someType, foos: [ { a: 1 }, { type: someOtherType, b: 2 } ] };

    declareType(someType, defaultContext, someResolvedType, { foos: someContext });
    declareType(undefined, someContext, 'typeA');
    declareType(someOtherType, someContext, 'typeB');

    testIt(docdef, {
      state: OK,
      context: defaultContext,
      input: docdef,
      resolved: someResolvedType,
      nested: {
        foos: [
          {
            state: OK,
            context: someContext,
            input: docdef.foos[0],
            resolved: 'typeA',
            nested: {}
          },
          {
            state: OK,
            context: someContext,
            input: docdef.foos[1],
            resolved: 'typeB',
            nested: {}
          }
        ]
      }
    });
  });

  it('should fold the nodes with a provided function, if any', function () {
    var docdef = { type: someType, foos: [ { a: 1 }, { type: someType, b: 2 } ] };

    function foldNode(parseNode) {
      var badNode = parseNode.state !== traversalHelper.OK;
      return badNode ? 'BAD_NODE' : parseNode.nested;
    }

    declareType(someType, defaultContext, someResolvedType, { foos: defaultContext });

    testIt(docdef, { fold: foldNode }, {
      foos: [
        'BAD_NODE',
        {
          foos: []
        }
      ]
    });
  });

});
