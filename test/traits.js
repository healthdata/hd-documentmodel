/* Original work Copyright (c) 2013-2014 Skryv
 * Modified work Copyright (c) 2014-2016 Healthdata.be
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
'use strict';

var traits = require('../lib/traits');

describe('traits', function () {

  describe('provides()', function () {

    it('should check whether a symbol is provided by a trait', function () {
      var t = { provides: [ 'foo' ] };
      expect(traits.provides(t, 'foo')).to.be.true;
      expect(traits.provides(t, 'bar')).to.be.false;
    });
  });

  describe('compose()', function () {

    it('should combine provided symbols of traits', function () {
      var t1 = { provides: [ 'foo' ] };
      var t2 = { provides: [ 'bar' ] };
      var t = traits.compose(t1, t2);
      expect(traits.provides(t, 'foo')).to.be.true;
      expect(traits.provides(t, 'bar')).to.be.true;
      expect(traits.provides(t, 'baz')).to.be.false;
    });

    it('should refuse conflicting provided symbols', function () {
      var t1 = { provides: [ 'foo' ] };
      var t2 = { provides: [ 'foo' ] };
      expect(function () {
        traits.compose(t1, t2);
      }).to.throw('traits.compose: traits should have disjoint "provides" properties');
    });
  });

  describe('empty()', function () {

    it('should not provide anything', function () {
      var t = traits.empty();
      expect(traits.provides(t, 'foo')).to.be.false;
    });
  });

});
